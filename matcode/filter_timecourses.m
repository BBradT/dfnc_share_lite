function TCfilt = filter_timecourses(TC,cutoff,TR)
% FILTERS AND ZSCORES THE DETRENDED TC
% TC is subjects*time*components
%  cutoff is as 2 real values positive values for example [ 0.01  0.1]
%  TR is in seconds
%------------------------------------------------
%outputdir = '/export/mialab/hcp/dynamics';
%% not corrected for motion 
%inname = '/export/mialab/hcp/dynamics/TC_detrended.mat';
%outname = 'TC_detrended_filtered.mat';
%% corrected for motion params
%inname = '/export/mialab/hcp/dynamics/TC_detrended_mortho.mat';
%outname = 'TC_detrended_mortho_filtered.mat';
%% corrected for motion and "scrubbed"
%inname = '/export/mialab/hcp/dynamics/TC_scrubbed.mat';
%outname = 'TC_scrubbed_filtered.mat';
%% load in the detrended/despiked timecourses
%load(inname); % loads TC [M x T x C]

%% filter timeseries
% TR = 2; % seconds
%HFcutoff = 0.15;
NyqF = (1/TR)/2;
Wn = cutoff./NyqF;


%% define the filter
N=5;
%[B,A]=butter(N,Wn,'low');
[B,A]=butter(N,Wn);


TCfilt = zeros(size(TC));
for m = 1:size(TC,1)
    fprintf('Working on subject %d of %d\n', m, size(TC,1))
    for c = 1:size(TC,3)
        TCfilt(m,:,c)= zscore(filtfilt(B,A,TC(m,:,c)));
    end
end

%TC = TCfilt;

%fprintf('saving to disk\n')
%save(fullfile(outputdir, outname), 'TC')

