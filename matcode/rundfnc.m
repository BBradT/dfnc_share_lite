function [dFNC] = rundfnc(tc, wsize, window_alpha, metric)
% Compute sliding windows for dFNC with Gaussian Convolution
%
% usage:
%	[dFNC] = run_dfnc(tc);
% 		tc = network timecourses : N x TC x IC
%		dFNC = subject sliding windows : N x (TC - wsize) x (IC^2/2-IC)
%
% Derived from code by Anees Abrol
% modified version of GIFT icatb_run_dfnc.m
% tc: network time-courses
% wsize: in TRs 
% window_alpha: size of Gaussian convolved with in TRs 
%
% Requires Mialab ICA toolbox to run

% Run dfnc
icatb_defaults;
% global DETRENDNUMBER;

% Initialise
% prefix = 'test_dfnc';
TR = 2; % TR in seconds
doDespike = 'no';
tc_filter = 0; % Cutoff in Hz
method = 'none';
if ~exist('wsize','var')
     wsize = 44;
end
if ~exist('metric','var')
     metric = 'cov';
end
if ~exist('window_alpha', 'var')
     window_alpha = 1;
end
%num_repetitions = 10;

numOfSub = size(tc, 1);
if (length(size(tc)) == 3)
    numOfSess = 1;
    nT = size(tc, 2);
    numComp = size(tc, 3);
else
    numOfSess = size(tc, 2);
    nT = size(tc, 3);
    numComp = size(tc, 4);
end

comps = (1:numComp);

disp('----------------------------------------------------');
disp('-------------- STARTING DYNAMIC FNC --------------');
disp('----------------------------------------------------');

fprintf('\n');

TR = min(TR);

% Make sliding window
numComp = length(comps);
c = compute_sliding_window(nT, window_alpha, wsize);
A = repmat(c, 1, numComp);

Nwin = nT - wsize;

tc = reshape(tc, numOfSub, numOfSess, nT, numComp);

dataSetCount = 0;

dFNC = zeros(numOfSub, numOfSess,Nwin, nchoosek(numComp,2)) ;
% Loop over subjects
for nSub = 1:numOfSub
    
    % Loop over sessions
    for nSess = 1:numOfSess
        
        dataSetCount = dataSetCount + 1;
                
        FNCdyn = zeros(Nwin, nchoosek(numComp,2));
        disp(['Computing dynamic FNC on subject ', num2str(nSub), ' session ', num2str(nSess)]);
                
        % Preprocess timecourses
        for nComp = 1:numComp
            current_tc = squeeze(tc(nSub, nSess, :, nComp));
            
            % Despiking timecourses
            if (strcmpi(doDespike, 'yes'))
                current_tc = icatb_despike_tc(current_tc, TR);
            end
            
            % Filter timecourses
            if (tc_filter > 0)
                current_tc = icatb_filt_data(current_tc, TR, tc_filter);
            end
            tc(nSub, nSess, :, nComp) = current_tc;
        end
        
        % Apply circular shift to timecourses
        tcwin = zeros(Nwin, nT, numComp);
        for ii = 1:Nwin
            Ashift = circshift(A, round(-nT/2) + round(wsize/2) + ii);
            tcwin(ii, :, :) = squeeze(tc(nSub, nSess, :, :)).*Ashift;
        end
        
        if strcmpi(method, 'none')

            % No L1
            for ii = 1:Nwin
                m = squeeze(tcwin(ii,:,:));
                if strcmpi(metric,'corr')
                     a = icatb_corr(m);
                elseif strcmpi(metric, 'mmi')
                     a = zeros(size(m,2));
                     for i = 1:size(m,2) 
                         for j = 1:size(m,2)
                             [a(i,j) nmi] =RMI(m(:,i),m(:,j),1);
                         end
                     end
                elseif strcmpi(metric, 'nmi')
                     a = zeros(size(m,2));
                     for i = 1:size(m,2) 
                         for j = 1:size(m,2)
                             [mi a(i,j)] = RMI(uint8(m(:,i)),uint8(m(:,j)));
                         end
                     end
                else
                     a = icatb_cov(m);
                end
                FNCdyn(ii, :) = mat2vec(a);
            end
            FNCdyn = atanh(FNCdyn);
            
        end
        
        dFNC(nSub, nSess,:,:) = FNCdyn;
        
        fprintf('\n');
        
    end
    % End of loop over sessions
end
% End of loop over subjects
if numOfSess == 1
    dFNC = permute(dFNC, [2 1 3 4]);
    dFNC = squeeze(dFNC);
end
disp('Analysis Complete');
fprintf('\n');


