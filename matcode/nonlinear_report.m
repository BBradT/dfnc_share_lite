load results/nonlinear_final.mat;
addpath(genpath('matcode/htmltables'));
%% Connectivity
% In this section, our aim is to investigate how Mutual-Information based
% measures can provide insights into differences between patient and
% healhty control connectivity. We first perform a high-level analysis of
% the differences in the resulting connectivity matrices, noting how the
% structure of the matrices is altogether 

[L, RSN, Art, MOD] = comp_labels_fb_C100_vn_FINAL;
L = L(RSN);

%%%
% First, we plot the static connectivity matrices using the standard
% correlation metric. We compute the correlation between the 47 
% Independent Components (ICs) estimated in Damaraju et al. 2015, and
% provide connectivity plots for each population separately.
% We also plot the mean P-values over the entire population for computating.
% correlation of each components

%%%
% Next, we plot the static connectivity matrices using the mutual
% information. We compute mutual information between the 47 
% Independent Components (ICs) estimated in Damaraju et al. 2015, and
% provide connectivity plots for each population separately.

%%%
% Then, we plot the static connectivity matrices using normalized mutual
% information. We compute normalized mutual information between the 47 
% Independent Components (ICs) estimated in Damaraju et al. 2015, and
% provide connectivity plots for each population separately.


mRhc = squeeze(mean(Rhc,1));
mRhc(1:1+size(mRhc,1):end) = min(min(mRhc));

mRsz = squeeze(mean(Rsz,1));
mRsz(1:1+size(mRsz,1):end) = min(min(mRsz));

Lo=min(min(min(mRhc)),min(min(mRsz)));
Hi=max(max(max(mRhc)),max(max(mRsz)));

[fig, A, C, I] = plot_FNC(mRhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Control Standard Static Functional Network Connectivity using Correlation.');
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'Corr_HC.png');

[fig, A, C, I] = plot_FNC(mRsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Control Standard Static Functional Network Connectivity using Correlation.');
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'Corr_SZ.png');

mPhc = squeeze(mean(Phc,1));
mPsz = squeeze(mean(Psz,1));
Lo=min(min(min(mPhc)),min(min(mPsz)));
Hi=max(max(max(mPhc)),max(max(mPsz)));

[fig, A, C, I] = plot_FNC(mPhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy P-Values for correlation.');
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);

[fig, A, C, I] = plot_FNC(mPsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Patient P-Values for correlation.');
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);


mMhc = squeeze(mean(Mhc,1));
mMhc(1:1+size(mMhc,1):end) = min(min(mMhc));
mMsz = squeeze(mean(Msz,1));
mMsz(1:1+size(mMsz,1):end) = min(min(mMsz));

Lo=min(min(min(mMhc)),min(min(mMsz)));
Hi=max(max(max(mMhc)),max(max(mMsz)));

[fig, A, C, I] = plot_FNC(mMhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Control Static Functional Network Connectivity using Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'Mi_HC.png');

[fig, A, C, I] = plot_FNC(mMsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Patient Static Functional Network Connectivity using Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'Mi_SZ.png');

% Next, we plot Normalized Mutual Information
mNhc = squeeze(mean(Nhc,1));
mNhc(1:1+size(mNhc,1):end) = min(min(mNhc));
mNsz = squeeze(mean(Nsz,1));
mNsz(1:1+size(mNsz,1):end) = min(min(mNsz));
Lo=min(min(min(mNhc)),min(min(mNsz)));
Hi=max(max(max(mNhc)),max(max(mNsz)));

[fig, A, C, I] = plot_FNC(mNhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Static Functional Network Connectivity using Normalized Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'Nmi_HC.png');

[fig, A, C, I] = plot_FNC(mNsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Patient Static Functional Network Connectivity using Normalized Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'Nmi_SZ.png');

%mPhc = squeeze(median(Phc,1));
%Lo=min(min(mPhc));
%Hi=max(max(mPhc));
%[fig, A, C, I] = plot_FNC(mPhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
%title 'Median Correlation PValues';
YresHCf = zeros(size(hc,1));
YresHC = zeros(size(hc,1),NC,NC);
YresSZ = zeros(size(sz,1),NC,NC);

for i = 1:size(hc,1)
    fprintf('Healthy %d\n',i);
    for c1=1:NC
        sc1 = squeeze(hc(i,:,c1));
        for c2=1:NC
            sc2 = squeeze(hc(i,:,c2));
            b1 = Rhc(i,c1,c2);%sc2'\sc1';
            y=b1*sc1;
            YresHC(i,c1,c2) = norm(sc2-y);
            YresHC(i,c2,c1) = norm(sc2-y);
        end
    end
end

for i = 1:size(sz,1)
    fprintf('Patient %d\n',i);
    for c1=1:NC
        sc1 = squeeze(sz(i,:,c1));
        for c2=1:NC
            sc2 = squeeze(sz(i,:,c2));
            b1 = Rsz(i,c1,c2);%sc2'\sc1';
            y=b1*sc1;
            YresSZ(i,c1,c2) = norm(sc2-y);
        end
    end
end


YresHCm = squeeze(mean(YresHC,1));
YresSZm = squeeze(mean(YresSZ,1));
Lo=min(min(min(YresHCm)),min(min(YresSZm)));
Hi=max(max(max(YresHCm)),max(max(YresSZm)));
YresHCm(logical(eye(size(YresHCm)))) = Hi;
YresSZm(logical(eye(size(YresSZm)))) = Hi;
Lo=min(min(min(YresHCm)),min(min(YresSZm)));
Hi=max(max(max(YresHCm)),max(max(YresSZm)));

[fig, A, C, I] = plot_FNC(YresHCm,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Controls - Residuals of Linear Trend.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'results/Res_HC.png');

[fig, A, C, I] = plot_FNC(YresSZm,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Patients - Residuals of Linear Trend.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'results/Res_SZ.png');

close all;
%% Finding Non-Linear Activations

for c1=14
    c1m = squeeze(median(P, 1));
    sc1 = squeeze(mean(hc(:,:,c1),1));
    sz1 = squeeze(mean(sz(:,:,c1),1));
    for c2=1:NC
        %if c2 <= c1
        %    continue;
        %end
        componentDir = sprintf('results/NL/NL-%dVS%d/',c1,c2);
        if ~exist(componentDir,'dir')        
            mkdir(componentDir);
        end
        fprintf('Comparing component %d with %d\n',c1,c2);
        sc2 = squeeze(mean(hc(:,:,c2),1));
        sz2 = squeeze(mean(sz(:,:,c2),1));

        %%% Regression Figures
        figure('visible','off');
        % Healthy
        b1 = mean(Rhc(:,c1,c2));%sc2'\sc1';
        y=b1*sc1;
        fm=ksr(sc1,sc2);
        ynl = fm.f;
        hold on;
        scatter(sc1,sc2,30,'.');
        plot(sc1,y,'LineWidth',4);
        plot(fm.x,fm.f,'LineWidth',4);
        xlabel(sprintf('Component %d', c1));
        ylabel(sprintf('Component %d', c2));
        set(gca,'FontSize',18);
        grid on;
        grid minor;
        axis square;
        legend({'Components', 'Linear Fit', 'NonLinear Fit'},'location','best','fontsize',12);
        %title(sprintf('Healthy Regression - Component %d vs %d', c1, c2));
        saveas(gcf,[componentDir sprintf('/hc_regression_%d_%d.png', c1,c2)]);
        % Patient
        figure('visible','off');
        b1 = mean(Rsz(:,c1,c2));%sc2'\sc1';
        y=b1*sz1;
        fm=ksr(sz1,sz2);
        ynl = fm.f;
        hold on;
        scatter(sz1,sz2,30,'.');
        plot(sz1,y,'LineWidth',4);
        plot(fm.x,fm.f,'LineWidth',4);
        xlabel(sprintf('Component %d', c1));
        ylabel(sprintf('Component %d', c2));
        grid on;
        grid minor;
        axis square;
        set(gca,'FontSize',18);
        legend({'Components', 'Linear Fit', 'NonLinear Fit'},'location','best','fontsize',12);
        %set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
        %title(sprintf('Patient Regression - Component %d vs %d', c1, c2));
        saveas(gcf,[componentDir sprintf('/sz_regression_%d_%d.png', c1,c2)]);
        
        %%% TC figure
        % Healthy
        figure('visible','off');
        subplot(1,2,1);
        hold on;
        plot(sc1,'LineWidth',3);
        plot(sc2,'LineWidth',3);
        grid on;
        grid minor;
        axis square;
        legend({sprintf('Component %d',c1),sprintf('Component %d',c2)},'location','best');
        title(sprintf('Healhty TimeCourses - Component %d vs %d', c1, c2));
        % Patient
        subplot(1,2,2);
        hold on;
        plot(sz1,'LineWidth',3);
        plot(sz2,'LineWidth',3);
        grid on;
        grid minor;
        axis square;
        legend({sprintf('Component %d',c1),sprintf('Component %d',c2)},'location','best');
        title(sprintf('Patient TimeCourses - Component %d vs %d', c1, c2));
        set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
        saveas(gcf,[componentDir sprintf('/timecourses_%d_%d.png', c1,c2)]); 
        
        %%% Printed Statistics
        c1Rhc = squeeze(Rhc(:,c1,c2));
        c1Phc = squeeze(Phc(:,c1,c2));
        c1Mhc = squeeze(Mhc(:,c1,c2));
        c1Nhc = squeeze(Nhc(:,c1,c2));
        c1Lhc = length(c1Rhc);
        c1Rsz = squeeze(Rsz(:,c1,c2));
        c1Psz = squeeze(Psz(:,c1,c2));
        c1Msz = squeeze(Msz(:,c1,c2));
        c1Nsz = squeeze(Nsz(:,c1,c2));
        c1Lsz = length(c1Rsz);

        fid = fopen([componentDir sprintf('/stats_%d_%d.txt',c1,c2)],'w');
        %fprintf(fid,'Mean Correlation Between %d and %d:  %f\n',c1,c2, mean(c1R));
        %fprintf(fid,'Median Correlation Between %d and %d:  %f\n', c1,c2,median(c1R));
        %fprintf(fid,'Mean MI Between %d and %d:  %f\n',c1,c2, mean(c1M));
        %fprintf(fid,'Median MI Between %d and %d:  %f\n', c1,c2,median(c1M));
        %fprintf(fid,'Mean NMI Between %d and %d:  %f\n',c1,c2, mean(c1N));
        %fprintf(fid,'Median NMI Between %d and %d:  %f\n', c1,c2,median(c1N));
        %fprintf(fid,'Mean P-Value Between %d and %d:  %f\n',c1,c2, mean(c1P));
        %fprintf(fid,'Median P-Value Between %d and %d:  %f\n', c1,c2,median(c1P));
        %fprintf('\tFisher P-Value Between %d and %d:  %f\n', c1,c2,final_p_value);
        %fprintf(fid,'Number of |Correlations above 0.15| Between %d and %d:  %d/%d\n', c1,c2, sum(abs(c1R)>0.15),c1L);
        %fprintf(fid,'Number of |Correlations below 0.15| Between %d and %d:  %d/%d\n', c1,c2, sum(abs(c1R)<0.15),c1L);
        %fprintf(fid,'Number of Significant Comparisons Between %d and %d:  %d/%d\n', c1,c2, sum(c1P<0.05),c1L);
        %fprintf(fid,'Number of Insignificant Comparisons Between %d and %d:  %d/%d\n', c1,c2, sum(c1P>0.05),c1L);

        %%% Correlation Histogram
        figure('visible','off');
        % Healthy
        h=histfit(c1Rhc,20,'kernel');
        set(h(1),'facealpha',0.5);
        set(h(2),'color','k','marker','^','linewidth',1,'linestyle','-');
        hold on;
        v1=vline(mean(c1Rhc),'r-');
        set(v1,'LineWidth',3);
        v2=vline(median(c1Rhc),'b');
        set(v2,'LineWidth',3);
        grid on;
        grid minor;
        plot(NaN,'r','LineWidth',3);
        plot(NaN,'b','LineWidth',3);
        axis square;
        %legend({'Frequency','Fit','Mean','Median'},'location','best');
        % Patient
        h=histfit(c1Rsz,20,'kernel');
        set(h(1),'facealpha',0.5);
        set(h(2),'color','m','marker','o','linewidth',1,'linestyle','-.');
        hold on;
        v1=vline(mean(c1Rsz),'g-');
        set(v1,'LineWidth',3);
        v2=vline(median(c1Rsz),'c');
        set(v2,'LineWidth',3);
        grid on;
        grid minor;
        plot(NaN,'g','LineWidth',3);
        plot(NaN,'c','LineWidth',3);
        axis square;
        legend({'HC Frequency','HC Fit','HC Mean','HC Median', 'SZ Frequency','SZ Fit','SZ Mean','SZ Median'},'location','best');
        title(sprintf('Correlation - Component %d vs %d', c1, c2));
        saveas(gcf,[componentDir sprintf('/correlation_histogram_%d_%d.png', c1,c2)]);
        
        %%% Mutual Information0
        figure('visible','off');
        % Healthy
        h=histfit(c1Mhc,20,'kernel');
        set(h(1),'facealpha',0.5);
        set(h(2),'color','k','marker','^','linewidth',1,'linestyle','-');
        hold on;
        v1=vline(mean(c1Mhc),'r-');
        set(v1,'LineWidth',3);
        v2=vline(median(c1Mhc),'b');
        set(v2,'LineWidth',3);
        grid on;
        grid minor;
        plot(NaN,'r','LineWidth',3);
        plot(NaN,'b','LineWidth',3);
        axis square;
        % Patient
        h=histfit(c1Msz,20,'kernel');
        set(h(1),'facealpha',0.5);
        set(h(2),'color','m','marker','o','linewidth',1,'linestyle','-.');
        hold on;
        v1=vline(mean(c1Msz),'g-');
        set(v1,'LineWidth',3);
        v2=vline(median(c1Msz),'c');
        set(v2,'LineWidth',3);
        grid on;
        grid minor;
        plot(NaN,'g','LineWidth',3);
        plot(NaN,'c','LineWidth',3);
        axis square;
        legend({'HC Frequency','HC Fit','HC Mean','HC Median', 'SZ Frequency','SZ Fit','SZ Mean','SZ Median'},'location','best');
        title(sprintf('Patient Mutual Information - Component %d vs %d', c1, c2));
        %set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
        saveas(gcf,[componentDir sprintf('/mutual_info_histogram_%d_%d.png', c1,c2)]);
        
        %%% NMI
        figure('visible','off');
        % Healhty
        h=histfit(c1Nhc,20,'kernel');
        set(h(1),'facealpha',0.5);
        set(h(2),'color','k','marker','^','linewidth',1,'linestyle','-');
        hold on;
        v1=vline(mean(c1Nhc),'r-');
        set(v1,'LineWidth',3);
        v2=vline(median(c1Nhc),'b');
        set(v2,'LineWidth',3);
        grid on;
        grid minor;
        plot(NaN,'r','LineWidth',3);
        plot(NaN,'b','LineWidth',3);
        axis square;
        % Patient
        h=histfit(c1Nsz,20,'kernel');
        set(h(1),'facealpha',0.5);
        set(h(2),'color','m','marker','o','linewidth',1,'linestyle','-.');
        hold on;
        v1=vline(mean(c1Nsz),'r-');
        set(v1,'LineWidth',3);
        v2=vline(median(c1Nsz),'b');
        set(v2,'LineWidth',3);
        grid on;
        grid minor;
        plot(NaN,'r','LineWidth',3);
        plot(NaN,'b','LineWidth',3);
        axis square;
        legend({'HC Frequency','HC Fit','HC Mean','HC Median', 'SZ Frequency','SZ Fit','SZ Mean','SZ Median'},'location','best');
        title(sprintf('NMI - Component %d vs %d', c1, c2));
        %set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
        saveas(gcf,[componentDir sprintf('/nmi_histogram_%d_%d.png', c1,c2)]);

        %%% Pvalues
        figure('visible','off');
        % Healthy
        h=histfit(c1Phc,20,'kernel');
        set(h(1),'facealpha',0.5);
        hold on;
        set(h(2),'color','k','marker','^','linewidth',1,'linestyle','-');
        v1=vline(mean(c1Phc),'r-');
        set(v1,'LineWidth',3);
        v2=vline(median(c1Phc),'b');
        set(v2,'LineWidth',3);
        grid on;
        grid minor;
        plot(NaN,'r','LineWidth',3);
        plot(NaN,'b','LineWidth',3);
        axis square;
        % Patient
        h=histfit(c1Psz,20,'kernel');
        set(h(1),'facealpha',0.5);
        set(h(2),'color','m','marker','o','linewidth',1,'linestyle','-.');
        hold on;
        v1=vline(mean(c1Psz),'g-');
        set(v1,'LineWidth',3);
        v2=vline(median(c1Psz),'c');
        set(v2,'LineWidth',3);
        grid on;
        grid minor;
        plot(NaN,'g','LineWidth',3);
        plot(NaN,'c','LineWidth',3);
        axis square;
        title(sprintf('Correlation P-Values - Component %d vs %d', c1, c2));
        legend({'HC Frequency','HC Fit','HC Mean','HC Median', 'SZ Frequency','SZ Fit','SZ Mean','SZ Median'},'location','best');        
        %set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
        saveas(gcf,[componentDir sprintf('/pvalue_histogram_%d_%d.png', c1,c2)]);
        
        %autoArrangeFigures;
        close all;

    end
end


