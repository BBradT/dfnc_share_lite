function [RMAT, mINFO, Rmax, mSign]  = best_greedy_match_corr(refmaps, newmaps)
RMAT = corr(refmaps, newmaps).^2;
nC = min(size(refmaps,2), size(newmaps,2));
mINFO = zeros(nC,2);
Rmax = zeros(nC,1);
mSign = zeros(nC,1);
RMATii = RMAT; 
reduced_ind_ref = 1:nC;
reduced_ind_new = 1:nC;
%fprintf('Ref\t\t New\t\t R^2\t\t Sign\n')
for ii = 1:nC
    mv = max(RMATii(:));
    [refind, newind] = find(RMATii == mv, 1, 'first');
    mINFO(ii,:) = [reduced_ind_ref(refind), reduced_ind_new(newind)];
    Rmax(ii) = mv;
    mSign(ii) = sign(corr(refmaps(:,reduced_ind_ref(refind)), newmaps(:,reduced_ind_new(newind))));
    %fprintf('%d\t\t %d\t\t %0.4f\t\t %d\n',reduced_ind_ref(refind), reduced_ind_new(newind), mv,  mSign(ii))
    reduced_ind_ref = setdiff(reduced_ind_ref, reduced_ind_ref(refind));
    reduced_ind_new = setdiff(reduced_ind_new, reduced_ind_new(newind));
    RMATii = RMAT(reduced_ind_ref,reduced_ind_new);
end
