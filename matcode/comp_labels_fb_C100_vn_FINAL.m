function [L, RSN_I, ART_I, fmod_RSN] = comp_labels_fb_C100_vn_FINAL
% [L, RSN_I] = comp_labels
% L  = a cell array of component labels 
% RSN_I = indices of RSNs, ordered by families
%% Define the labels
L = { ...
'Putamen'; ...              %  1  RSN
'Putamen'; ...              %  2  RSN
'motion'; ...               %  3  ART
'pulsation'; ...            %  4  ART
'PreCG'; ...                %  5  RSN
'R PoCG'; ...               %  6  RSN 
'Cuneus'; ...               %  7  RSN
'WM'; ...                   %  8  ART
'ParaCL'; ...               %  9  RSN
'L PoCG'; ...               % 10  RSN
'motion'; ...               % 11  ART
'PCC'; ...                  % 12  RSN
'Caudate'; ...              % 13  RSN
'sinus'; ...                % 14  mixed(RSN OC/AN??)
'sinus'; ...                % 15  ART
'motion'; ...               % 16  ART
'WM'; ...                   % 17  ART
'Thalamus'; ...             % 18  RSN
'sinus'; ...                % 19  mixed (SMA)
'MOG'; ...                  % 20  RSN
'MiFG'; ...                 % 21  RSN
'internal capsule'; ...     % 22  ART
'vents'; ...                % 23  ART
'SPL'; ...                  % 24  RSN
'WM/susceptibility?'; ...   % 25  ART
'vents'; ...                % 26  ART
'vents'; ...                % 27  ART
'IFG'; ...                  % 28  RSN
'vascular'; ...             % 29  ART
'Precuneus'; ...            % 30  RSN
'WM'; ...                   % 31  ART
'pulsation'; ...            % 32  ART
'WM'; ...                   % 33  ART
'IFG'; ...                  % 34  RSN
'Precuneus'; ...            % 35  RSN
'sinus'; ...                % 36  mixed (paracentral)
'motion'; ...               % 37  ART
'WM'; ...                   % 38  ART
'sinus'; ...                % 39  mixed (R RO)
'Precuneus'; ...            % 40  RSN
'Insula'; ...               % 41  RSN
'MT/MOG'; ...               % 42  RSN
'Calcarine'; ...           % 43  RSN
'sinus'; ...                % 44  ART
'WM'; ...                   % 45  ART
'CB'; ...                 % 46  RSN
'PCC'; ...                  % 47  RSN
'WM'; ...                   % 48  ART
'WM'; ...                   % 49  ART
'WM'; ...                   % 50  ART
'STG'; ...                  % 51  RSN
'WM'; ...                   % 52  ART
'ACC'; ...                  % 53  RSN
'motion'; ...               % 54  ART
'WM'; ...                   % 55  ART
'WM'; ...                   % 56  ART
'FFG'; ...                  % 57  RSN
'STG'; ...                  % 58  RSN
'PoCG'; ...                 % 59  RSN
'MOG'; ...                  % 60  RSN
'L MTG+IFG'; ...            % 61  RSN
'WM'; ...                   % 62  ART
'ITG'; ...                  % 63  RSN
'WM'; ...                   % 64  ART
'IFG'; ...                  % 65  RSN
'R IPL'; ...                % 66  RSN
'sinus'; ...                 % 67  mixed (L RO)
'WM'; ...                   % 68  ART
'dMPFC'; ...                % 69  RSN
'motion'; ...               % 70  ART
'motion'; ...               % 71  ART
'vents'; ...                % 72  ART
'WM'; ...                   % 73  ART
'vSMA'; ...                 % 74  RSN
'SN'; ...                   % 75  RSN
'Lingual'; ...             % 76  RSN
'sinus'; ...                % 77  ART
'Cuneus'; ...               % 78  RSN
'sinus'; ...                % 79  mixed (MCC)
'MTG'; ...                  % 80  RSN
'sinus'; ...                % 81  ART
'motion'; ...               % 82  ART
'WM'; ...                   % 83  ART
'R AG'; ...                 % 84  RSN
'WM'; ...                   % 85  ART
'motion'; ...               % 86  ART
'WM'; ...                   % 87  ART
'CB'; ...                   % 88  RSN
'SMG'; ...                  % 89  RSN
'AG'; ...                   % 90  RSN
'R Lingual'; ...            % 91  RSN
'WM'; ...                   % 92  ART
'sinus'; ...                % 93  ART
'L IPL'; ...                % 94  RSN
'L AG'; ...                 % 95  RSN
'SMG'; ...                  % 96  RSN
'WM'; ...                   % 97  ART
'WM'; ...                   % 98  ART
'motion'; ...               % 99  ART
'motion'; ...               % 100 ART
};

load orderedRSN_by_modularity

fmod_RSN = mfmod_final;
RSN_I = mRSN_final_ord;

%fmod_RSN = [ 5 2 6 10 9 7 6 2];
% RSN_I = [ ...
% % sub cortical  (5)
% % 14; ...  % RSN OC ??
% 75; ...  % RSN SN ??
% 13; ...  % RSN caudate
%  1; ...  % RSN putamen head
%  2; ...  % RSN putamen tail
% 18; ...  % RSN thalamus
% % auditory  (2)
% 58; ...  % RSN STG2
% 51; ...  % RSN STG2
% % somato sensory  (6)
%  5; ...  % RSN ventral motor
%  6; ...  % RSN r motor
% 10; ...  % RSN l motor
% 74; ...  % RSN vSMA
%  9; ...  % RSN paracentral
% 59; ...  % RSN Bi Postcentral
% % 36; ...  % RSN paracentral ???
% % 79; ...  % RSN MCC
% % visual (10)
% 76; ...  % RSN Lingual
% 78; ...  % RSN d Cuneus
% 43; ...  % RSN Calcarine
%  7; ...  % RSN v Cuneus (heart)
% 57; ...  % RSN FFG
% 91; ...  % RSN R MOG
% 42; ...  % RSN MT/MOG
% 20; ...  % RSN IOG
% 60; ...  % RSN SOG
% 12; ...  % RSN Calcarine
% % attn (9)
% 80; ...  % RSN MTG
% 24; ...  % RSN SPL
% 66; ...  % RSN R IPL
% 94; ...  % RSN L IPL
% 89; ...  % RSN IPL
% 63; ...  % RSN ITG
% 35; ...  % RSN Precuneus
% 40; ...  % RSN Precuneus
% 96; ...  % RSN SMG
% % front (7)
% 41; ...  % RSN pInsula
% 28; ...  % RSN aInsula 
% 34; ...  % RSN precentral
% 21; ...  % RSN MiFG
% 61; ...  % RSN L MTG+IFG
% 65; ...  % RSN IFG
% % 39; ...  % RSN R RO
% % 67; ...  % RSN L RO
% 69; ...  % RSN dMPFC
% % default-mode (6)
% 30; ...  % RSN PCC
% 47; ...  % RSN PCC
% 84; ...  % RSN R AG
% 95; ...  % RSN L AG
% 90; ...  % RSN AG
% 53; ...  % RSN ACC 
% %81; ...  % RSN PHG?? 
% % cereb (2)
% 46; ... % RSN cereb ?
% 88; ... % RSN cereb ?
% ];
ART_I = setdiff(1:length(L),RSN_I);
[L_art srind] = sort(L(ART_I));
ART_I = ART_I(srind);
ART_I = ART_I(:);
%  ART_I = [ ...
%      72; ...
%      26; ...
%      22; ...
%      15; ...
%      33; ...
%      68; ...
%      25; ...
%      27; ...
%      85; ...
%      48; ...
%      73; ...
%      11; ...
%      16; ...
%      70; ...
%      37; ...
%      99; ...
%      4; ...
%      32; ...
%      29; ...
%      44; ...
%  ];