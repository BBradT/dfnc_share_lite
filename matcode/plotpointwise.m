close all;
load results/pointwise_analysis.mat
figure('units','normalized','outerposition',[0,0,1,1]);
hold on;
Lo=min(min(min(sz_e)),min(min(hc_e)));
Hi=max(max(max((sz_e)),max(max(hc_e))));
ax=gca;
p=boxplot(gca,sz_e,1:47,'jitter',0.5,'width',1,'Colors','r','BoxStyle','filled','symbol','+r');
set(gca,'FontSize',18);
%set(p,'LineWidth',2);
title('SZ');
ylim([Lo,Hi]);
xlabel('Components','FontSize',12);
ylabel('Entropy');
xticklabels(L);
xtickangle(90);
grid on;
grid minor;

p=boxplot(gca,hc_e,1:47,'jitter',0.5,'width',2,'Colors','b','BoxStyle','filled','symbol','+b');
set(gca,'FontSize',18);
%set(p,'LineWidth',2);
title('HC');
ylim([Lo,Hi]);
xlabel('Components','FontSize',12);
ylabel('Entropy');
xticklabels(L);
xtickangle(90);
plot(Lo,'r-','LineWidth',2);
plot(Lo,'b-','LineWidth',2);
legend({'SZ','HC'},'location','best');

saveas(gcf,'componentwise_entropy.png');


figure('units','normalized','outerposition',[0,0,1,1]);
hold on;
subplot(3,3,1);
boxplot(squeeze(mean(szsz_corr,1)));
ylabel('PCC');
title('SZ v SZ');
set(gca,'FontSize',18);

subplot(3,3,2);
boxplot(squeeze(mean(szhc_corr,1)));
title('SZ v HC');
set(gca,'FontSize',18);

subplot(3,3,3);
boxplot(squeeze(mean(hchc_corr,1)));
title('HC v HC');
set(gca,'FontSize',18);

subplot(3,3,4);
boxplot(squeeze(mean(szsz_mi,1)));
title('SZ v SZ');
ylabel('MI');
set(gca,'FontSize',18);

subplot(3,3,5);
boxplot(squeeze(mean(szhc_mi,1)));
title('SZ v HC');
set(gca,'FontSize',18);

subplot(3,3,6);
boxplot(squeeze(mean(hchc_mi,1)));
title('HC v HC');
set(gca,'FontSize',18);

subplot(3,3,7);
boxplot(squeeze(mean(szsz_nmi,1)));
title('SZ v SZ');
ylabel('NMI');
set(gca,'FontSize',18);

subplot(3,3,8);
boxplot(squeeze(mean(szhc_nmi,1)));
title('SZ v HC');
set(gca,'FontSize',18);

subplot(3,3,9);
boxplot(squeeze(mean(hchc_nmi,1)));
title('HC v HC');
set(gca,'FontSize',18);

saveas(gcf,'pointwise_analysis.png');