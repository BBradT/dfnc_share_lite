load results/nonlinear_final.mat;
load data/parameters/DEMO_ICA_fBIRNp3_New;
addpath(genpath('matcode/htmltables'));
Fhcf = mat2vec(Fhc);
N = length(Fhcf);
Mhcf = zeros(size(hc,1), N);
Mszf = zeros(size(sz,1), N);
Nhcf = zeros(size(hc,1), N);
Nszf = zeros(size(sz,1), N);
Rhcf = zeros(size(hc,1), N);
Rszf = zeros(size(sz,1), N);

for i = 1:size(hc,1)
   Mhcf(i,:) = mat2vec(squeeze(Mhc(i,:,:))); 
   Nhcf(i,:) = mat2vec(squeeze(Nhc(i,:,:))); 
   Rhcf(i,:) = mat2vec(squeeze(Rhc(i,:,:))); 
end

for i = 1:size(sz,1)
   Mszf(i,:) = mat2vec(squeeze(Msz(i,:,:))); 
   Nszf(i,:) = mat2vec(squeeze(Nsz(i,:,:))); 
   Rszf(i,:) = mat2vec(squeeze(Rsz(i,:,:))); 
end

y = [zeros(size(hc,1), 1); ones(size(sz,1), 1)];

% Mutual Information Alone
X = [Mhcf; Mszf];
save('data/Mx.mat','X','y');

% Normalized Mutual Information Alone
X = [Nhcf; Nszf];
save('data/Nx.mat','X','y');

% Correlation Alone
X = [Rhcf; Rszf];
save('data/Rx.mat','X','y');

% Correlation and MI
X = [Rhcf Mhcf; Rszf Mszf];
save('data/RMx.mat','X','y');

% Correlation and NMI
X = [Rhcf Nhcf; Rszf Nszf];
save('data/RNx.mat','X','y');

% MI and NMI
X = [Mhcf Nhcf; Mszf Nszf];
save('data/MNx.mat','X','y');

% All three
X = [Rhcf Mhcf Nhcf; Rszf Mszf Nszf];
save('data/RMNx.mat','X','y');

