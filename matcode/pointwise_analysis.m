%% load
load data/fbirn_tc
load data/parameters/DEMO_ICA_fBIRNp3_New

%% init
TC=uint8_preservable(TC, 10);
sz=TC(DEMO.full(:,3)==1,:,:);
hc=TC(DEMO.full(:,3)==0,:,:);

Nsz = size(sz,1);
Nhc = size(hc,1);
T = size(sz,2);
NC = size(sz,3);

sz_e = zeros(Nsz,NC);
hc_e = zeros(Nhc,NC);
szsz_corr = zeros(Nsz,Nsz,NC);
szsz_nmi = zeros(Nsz,Nsz,NC);
szsz_je = zeros(Nsz,Nsz,NC);
szsz_mi = zeros(Nsz,Nsz,NC);
hchc_corr = zeros(Nhc,Nhc,NC);
hchc_nmi = zeros(Nhc,Nhc,NC);
hchc_je = zeros(Nhc,Nhc,NC);
hchc_mi = zeros(Nhc,Nhc,NC);
szhc_corr = zeros(Nsz,Nhc,NC);
szhc_nmi = zeros(Nsz,Nhc,NC);
szhc_mi = zeros(Nsz,Nhc,NC);


%% SZ vs
for i = 1:Nsz
    % sz v sz
    for j=1:Nsz
        if i > j
            continue;
        end
        for k=1:NC
            s1=squeeze(sz(i,:,k))';
            s2=squeeze(sz(j,:,k))';
            C=corr(s1,s2);
            szsz_corr(i,j,k)=C;
            szsz_corr(j,i,k)=C;
            [mi, nmi, ei, ej] =RMI(uint8(s1),uint8(s2));
            szsz_mi(i,j,k)=mi;
            szsz_mi(j,i,k)=mi;
            szsz_nmi(i,j,k)=nmi;
            szsz_nmi(j,i,k)=nmi;
            sz_e(i,k)=ei;
            sz_e(j,k)=ej;
        end
    end
    % sz vs hc
    for j=1:Nhc
        if i > j
            continue;
        end
        for k=1:NC
            s1=squeeze(sz(i,:,k))';
            s2=squeeze(hc(j,:,k))';
            C=corr(s1,s2);
            szhc_corr(i,j,k)=C;
            szhc_corr(j,i,k)=C;
            [mi, nmi]=RMI(uint8(s1),uint8(s2));
            szhc_mi(i,j,k)=mi;
            szhc_mi(j,i,k)=mi;
            szhc_nmi(i,j,k)=nmi;
            szhc_nmi(j,i,k)=nmi;
        end
    end
    fprintf('SZ vs - Done %d\n', i);    
end
%% HC vs
for i = 1:Nhc
    for j = 1:Nhc
        if i > j
            continue;
        end
        for k=1:NC
            s1=squeeze(hc(i,:,k))';
            s2=squeeze(hc(j,:,k))';
            C=corr(s1,s2);
            hchc_corr(i,j,k)=C;
            hchc_corr(j,i,k)=C;
            [mi, nmi,ei,ej]=RMI(uint8(s1),uint8(s2));
            hchc_mi(i,j,k)=mi;
            hchc_mi(j,i,k)=mi;
            hchc_nmi(i,j,k)=nmi;
            hchc_nmi(j,i,k)=nmi;
            hc_e(i,k)=ei;
            hc_e(j,k)=ej;
        end
    end
    fprintf('HC vs - Done %d\n', i);    
end

%% Saving

save('results/pointwise_analysis.mat', 'szsz_corr', 'szsz_nmi', ...
     'szsz_mi', 'szhc_corr', 'szhc_nmi', 'szhc_mi', 'hchc_corr', ...
     'hchc_mi','hchc_nmi','sz_e','hc_e');