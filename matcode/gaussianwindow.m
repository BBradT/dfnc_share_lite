function w = gaussianwindow(N,x0,sigma)
% Compute a gaussian window of size N
% 	with mean x0
%	and std sigma
%
% Usage: w = guassianwindow(N, x0, sigma) 
%

x = 0:N-1;
w = exp(- ((x-x0).^2)/ (2 * sigma * sigma))';
