clear;
fileFormat = 'Rcode/data/subject%04d.mat';
nSubj = 512;
loadSubj = @(x) evalin('base',sprintf('load(sprintf(fileFormat,%d));',x));

loadSubj(1);
[NC, T] = size(ts);

nlFuncs = {...
    @(x) x, ...
    @(x) x.^2 + x, ...
    @(x) x.^2, ...
    @(x) 2.^x, ...
    @(x) 2.^x + x.^2 ...
    };

nlFuncI = [1,2,3,4,5];

NCC = NC + length(nlFuncs);

TC = zeros(nSubj,T,NCC);

for i = 1:512
   loadSubj(i); 
   TC(i,:,1:NC) = ts';
   for ncc = 1:(NCC-NC)
       source = squeeze(ts(nlFuncI(ncc),:));
       dest = nlFuncs{ncc}(source);
       TC(i,:,NC+ncc) = dest; 
   end
   disp(i);
end

save('data/simulated_tcs.mat','TC');