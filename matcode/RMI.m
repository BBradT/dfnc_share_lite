function [mutual_information,rmi,entropy1,entropy2,joint_entropy] = RMI( im1, im2 )
% RMI takes two images and calculates the relative mutual information
% between the images im1 and im2
% The RMI is the mutual information of the two images divided by the min of
% the individual image entropies

% Convert the images to one dimensional arrays of doubles. This way we can
% calculate the MI of images with any dimension.
array1 = double(im1(:)) + 1;
array2 = double(im2(:)) + 1; 

% Should be the same size as indrow
if numel(array1) ~= numel(array2)
    error('Fatal Error: Input images are not the same size.');
end

% Super short method for creating the joint histogram
joint_histogram = accumarray([array1, array2], 1);

% Convert the histogram into a probability table
joint_prob = joint_histogram / numel(array1);

% Remove all the zeros from the array
joint_histogram_indicies = joint_histogram ~= 0;

% Recreate the joint probability table without zero entries
joint_prob = joint_prob(joint_histogram_indicies);

% Calculate the joint entropy
joint_entropy = -sum(joint_prob .* log2( joint_prob ));

entropy1 = entropy(im1);
entropy2 = entropy(im2);

% Compute mutual information using the built in entropy functions
mutual_information = entropy1 + entropy2 - joint_entropy;

% Compute the relative mutual information (RMI)
rmi = mutual_information/min(entropy1, entropy2);

end

