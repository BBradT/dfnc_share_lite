disp('Processing timecourses');
load ./data/parameters/COBRE_DEMO.mat % loads DEMO
%load ./data/parameters/motparfiles_fb314.mat % loads mp_list_fb314
nS = size(TC,1);
nT = zeros(1,nS);
for ii=1:nS, 
    nT(ii) = size(TC(ii,:,:),2);
end

nC = size(TC,3);
TCall = zeros(nS,nT(1)-4,nC);
mpv = zeros(nS,nT(1)-4,6);
for ii=1:nS
    mp = mpvals{ii};%load(mp_list_fb314{ii});
    if nT(ii) == 151 
        TCall(ii,:,:) = TC(ii,2:end,:);
        mpv(ii,:,:) = mp(2:end,:);
    else
        TCall(ii,:,:) = TC(ii,5:end,:);
        mpv(ii,:,:) = mp(1:end,:);
    end 
end

TR= 2;
prefix = 'fb_ICATC';
outputdir = './tmp/';
TC_spectral_metrics_modular(TCall,TR, prefix,outputdir,mpv);  

outTC = load('./tmp/fb_ICATC_timecourses_ica_det_orthomotspikei.mat');
rmat = zeros(nS,nC,nC);
for ii=1:nS,
   rmat(ii,:,:) = corr(squeeze(outTC.TCdet3(ii,:,:)));
end

cutoff = [ 0.01  0.15];
TCfilt = filter_timecourses(outTC.TCdet3,cutoff,TR);
rmatf = zeros(nS,nC,nC);
for ii=1:nS,
   rmatf(ii,:,:) = corr(squeeze(TCfilt(ii,:,:)));
end

clearvars DEMO TCall TR cutoff ii mp mp_list_fb314 mpv nC nS nT outTC outputdir prefix rmat rmatf

