function [TCnew, TCfit] = interp_despiked_timecourses(TC, ind,plotit)
% DESPIKES THE DETRENDED TC [T x C] (timePoints x Components)

%outputdir = '/export/mialab/hcp/dynamics';
%outname = 'TC_detrended_despiked.mat';
%inname = '/export/mialab/hcp/dynamics/TC_detrended.mat';

%addpath(genpath('/export/mialab/users/eallen/ALFF'))
addpath(genpath('/export/mialab/users/eswar/software/splinefit/'))
%% load in the detrended/despiked timecourses
%load(inname); % loads TC [T x C]
if nargin < 3
    plotit = 0;
end


TCnew = zeros(size(TC));
TCfit = zeros(size(TC));
for kk = 1:size(TC,2),
    tc = TC(:,kk);
    tc = tc(:);
    
    %% use a spline fit with few breakpoints to correct spikes that might also be end points
    % since the higher order spline fit doesn't deal well with those.
    if any(ismember(ind, [1, length(tc)]))
        xaxis = setdiff(1:length(tc),[ind]);
        ytemp = tc(xaxis);
        bpfrac = 0.05;
        sporder = 3;
        pp0 = splinefit(xaxis,ytemp,floor(length(tc)*bpfrac),sporder);  % Piecewise quadratic
        y0 = ppval(pp0,1:length(tc));
        tc(ismember(ind, [1, length(tc)])) = y0(ismember(ind, [1, length(tc)]));
    end
    
    
    %% Method from AFNI that just lowers the level of the spike
    %     s_out = s;
    %     for uu = 1:length(ind)
    %         if ind(uu) == 1 || ind(uu) == length(s)
    %             s_out(ind(uu)) = sign(s(ind(uu)))*(c1+((c2-c1)*tanh((abs(s(ind(uu)))-c1)/(c2-c1))));
    %         else
    %             %do nothing
    %         end
    %     end
    %    tc = yfit + s_out*sigma;
    
    %% remove those indices that have already been dealt with
    ind = setdiff(ind, [1,length(tc)]);
    
    
    %% use a spline fit to determine the new values
    xaxis = setdiff(1:length(tc),ind);
    ytemp = tc(xaxis);
    bpfrac = 0.5;
    sporder = 3;
    pp = splinefit(xaxis,ytemp,floor(length(tc)*bpfrac),sporder);  % Piecewise quadratic
    y1 = ppval(pp,1:length(tc));
    tcout = tc;
    tcout(ind) = y1(ind);
    %% Create a figure
    if plotit
    figure; plot(1:length(tc), TC(:,kk), 'k')
    hold on
    plot(ind, tc(ind), 'r*')
    plot(1:length(tc), tcout, 'g')
    hold on
    plot(1:length(tc), y1, ['r--'])
    end
    %% Store it
    TCnew(:,kk) = tcout; % just replace the "bad" points
    TCfit(:,kk) = y1; % fit from the spline
end
%disp(['finished subj: ' num2str(ii) 'in  time : ' num2str(toc) ' seconds' ])

%end






% function [yfit] = getSplineFit(estimates,len,TR)
% 
% numP = floor(len/30);
% t = 0:TR:(len-1)*TR;
% t = t(:);
% x0  = estimates;
% yfit = x0(1)*t + x0(2)*t.^2;
% for ii = 1:numP
%     yfit = yfit + x0(2+ii) * sin(2*pi*ii*t/(len*TR)) + x0(2+ii) *cos(2*pi*ii*t/(len*TR));
% end
% 
% 
% function [yfit] = getQuadFit(estimates,len,TR)
% 
% numP = floor(len/30);
% t = 0:TR:(len-1)*TR;
% t = t(:);
% x0  = estimates;
% yfit = x0(1)*t.^2 + x0(2)*t + x0(3);
