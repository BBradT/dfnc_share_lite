disp(sprintf('*********************\nStarting dFNC\n*********************'));

%close all;
NUM_SUBJ = 314;				% Number of Subjects to Load
% NUM_SUBJ = 204;  			% Maximum number of available COBRE Subjects
DATA_FOLDER = './data/fbirn/';		% Location of Data
REF_FILE = './data/parameters/fbirn_reference.mat';		% Location of Data
MASK_FILE = './data/parameters/fbirn_mask.mat';
RESULT_DIR = './results/';		% Location of Results
RESTRICT = 0;				% Restrict components by matching with known GT
NC = 100;				% Number of ICs to estimate
SUBJ_PC = 120;				% Number of Subject-Level PCs
NUM_STATES = 5;
load './data/parameters/DEMO_ICA_fBIRNp3_New.mat'
%load /export/mialab/users/eswar/fbirn_p3/results_C100_vn/TC/fbirnp3_rest_C100_ica_TC_scrubbed_filt_RSN.mat
load ./data/fbirn_tc.mat
TC_rsn_filt = TC;%permute(TC_rsn_filt,[2,3,1]);

[L, RSN, Art, MOD] = comp_labels_fb_C100_vn_FINAL;
L = L(RSN);
%% STEP 4 : Run dFNC to compute covariance matrices

%sfnc_corr = runsfnc(TC_rsn_filt,158,1,'corr');
%sfnc_mi = runsfnc(TC_rsn_filt,158,1,'mmi');
%sfnc_nmi = runsfnc(TC_rsn_filt,158,1,'nmi');
%sfnc_je = runsfnc(TC_rsn_filt,158,1,'je');
%sfnc_es = runsfnc(TC_rsn_filt,158,1,'es');
sfnc_emin = runsfnc(TC_rsn_filt,158,1,'emin');
sfnc_emax = runsfnc(TC_rsn_filt,158,1,'emax');
%sfnc_esn = runsfnc(TC_rsn_filt,158,1,'esn');

metrics = {
%    'Correlation', 
%    'Mutual-Information', 
%    'NMI', 
%    'Joint-Entropy', 
%    'Entropy-Sum',
    'Min-Entropy',
    'Max-Entropy',
%    'Sum-Norm'
};

sfnc = {
%    sfnc_corr,
%    sfnc_mi,
%    sfnc_nmi,
%    sfnc_je,
%    sfnc_es,
    sfnc_emin,
    sfnc_emax,
%    sfnc_esn
};
%metrices = metrics(6:7);
%sfnc = sfnc(6:7);

save('results/sfnc_results.mat','sfnc');

ranges = {[-1,1],[0,2],[0,1],[0,1],[0,1]};
figSize = [50, 50];            % [width, height]
figUnits = 'Centimeters';
%% PLOT FINAL STATES
for MET = 1:length(metrics)
disp(metrics{MET})
%% SFNC
HCu = sfnc{MET}(DEMO.full(:,3)==0,:);
SZu = sfnc{MET}(DEMO.full(:,3)==1,:);
HC = mean(HCu);
SZ = mean(SZu);
[h,p,ci,stats] = ttest2(HCu, SZu);
ts = stats.tstat;
Hi = max(max(HC),max(SZ));
Lo = min(min(HC),min(SZ));

[fig, A, C, I] = plot_FNC(vec2mat(HC),[Lo,Hi],L,1:47,'w',metrics{MET},MOD,'jet');
title(sprintf('HC %s',metrics{MET}));
filename = sprintf('./results/%s_HC_SFNC',metrics{MET});
print(filename ,'-dpng', '-r0');

[fig, A, C, I] = plot_FNC(vec2mat(SZ),[Lo,Hi],L,1:47,'w',metrics{MET},MOD,'jet');
title(sprintf('SZ %s',metrics{MET}));
%set(fig, 'Position', pos);
filename = sprintf('./results/%s_SZ_SFNC',metrics{MET});
print(filename ,'-dpng', '-r0');
DD=-log10(p).*ts;
Lo=min(min(DD));
Hi=max(max(DD));
[fig, A, C, I] = plot_FNC(DD,[Lo Hi],L,1:47,'w','-log10(p) * sign(t)',MOD,'jet');
%set(fig, 'Units', figUnits);
%pos = get(fig, 'Position');
%pos = [pos(1), pos(4)+figSize(2), pos(3)+figSize(1), pos(4)];
%fig.PaperPositionMode = 'auto';
title(sprintf('SZ-HC %s',metrics{MET}));
filename = sprintf('./results/%s_SsZ-HC_SFNC',metrics{MET});
print(filename ,'-dpng', '-r0');
%set(fig, 'Position', pos);
%filename = sprintf('./results/%s_SZHC_SFNC.pdf',metrics{MET});
%set(fig,'units','inches');

%set(fig,'PaperPosition',[0 0 12 12]);
%set(fig,'Position',[0 0 12 12]);
%print('-fillpage', fig, '-dpdf', filename );

%figs = cell(1,NUM_STATES);
%for i = 1:NUM_STATES
%    [figs{i},A,C,I] = plot_FNC(Call(i,:),[-0.04,0.04],L,1:47,'w','correlation',MOD,'jet');
%    title(sprintf('Centroid %d',i));
%end

%figSize = [21, 29];            % [width, height]
%figUnits = 'Centimeters';

%if ~exist('./results/', 'dir')
%    mkdir('./results/');
%end

%for f = 1:numel(figs)
%      fig = figs{f};

      % Resize the figure
%      set(fig, 'Units', figUnits);
%      pos = get(fig, 'Position');
%      pos = [pos(1), pos(4)+figSize(2), pos(3)+figSize(1), pos(4)];
%      set(fig, 'Position', pos);

      % Output the figure
%      filename = sprintf('./results/%s_State_%02d.pdf',metrics{MET}, f);
%      print( fig, '-dpdf', filename ,'-bestfit');

%end

save(sprintf('results/sfnc_results_%s.mat',metrics{MET}))

end

save('results/sfnc_results.mat')
