load results/nonlinear_final.mat;
addpath(genpath('matcode/htmltables'));
%% Connectivity
% In this section, our aim is to investigate how Mutual-Information based
% measures can provide insights into differences between patient and
% healhty control connectivity. We first perform a high-level analysis of
% the differences in the resulting connectivity matrices, noting how the
% structure of the matrices is altogether 

[L, RSN, Art, MOD] = comp_labels_fb_C100_vn_FINAL;
L = L(RSN);

%%%
% First, we plot the static connectivity matrices using the standard
% correlation metric. We compute the correlation between the 47 
% Independent Components (ICs) estimated in Damaraju et al. 2015, and
% provide connectivity plots for each population separately.
% We also plot the mean P-values over the entire population for computating.
% correlation of each components

%%%
% Next, we plot the static connectivity matrices using the mutual
% information. We compute mutual information between the 47 
% Independent Components (ICs) estimated in Damaraju et al. 2015, and
% provide connectivity plots for each population separately.

%%%
% Then, we plot the static connectivity matrices using normalized mutual
% information. We compute normalized mutual information between the 47 
% Independent Components (ICs) estimated in Damaraju et al. 2015, and
% provide connectivity plots for each population separately.


mRhc = squeeze(mean(Rhc,1));
mRhc(1:1+size(mRhc,1):end) = min(min(mRhc));

mRsz = squeeze(mean(Rsz,1));
mRsz(1:1+size(mRsz,1):end) = min(min(mRsz));

Lo=min(min(min(mRhc)),min(min(mRsz)));
Hi=max(max(max(mRhc)),max(max(mRsz)));

[fig, A, C, I] = plot_FNC(mRhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Control Standard Static Functional Network Connectivity using Correlation.');
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);

[fig, A, C, I] = plot_FNC(mRsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Control Standard Static Functional Network Connectivity using Correlation.');
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);

mPhc = squeeze(median(Phc,1));
mPsz = squeeze(median(Psz,1));
Lo=min(min(min(mPhc)),min(min(mPsz)));
Hi=max(max(max(mPhc)),max(max(mPsz)));

[fig, A, C, I] = plot_FNC(mPhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy P-Values for correlation.');
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf, 'results/HC_Median_Pvalues_Corr.png');

[fig, A, C, I] = plot_FNC(mPsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Patient P-Values for correlation.');
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'results/SZ_Median_Pvalues_Corr.png');

mMhc = squeeze(mean(Mhc,1));
mMhc(1:1+size(mMhc,1):end) = min(min(mMhc));
mMsz = squeeze(mean(Msz,1));
mMsz(1:1+size(mMsz,1):end) = min(min(mMsz));

Lo=min(min(min(mMhc)),min(min(mMsz)));
Hi=max(max(max(mMhc)),max(max(mMsz)));

[fig, A, C, I] = plot_FNC(mMhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Control Static Functional Network Connectivity using Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);

[fig, A, C, I] = plot_FNC(mMsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Patient Static Functional Network Connectivity using Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);

% Next, we plot Normalized Mutual Information
mNhc = squeeze(mean(Nhc,1));
mNhc(1:1+size(mNhc,1):end) = min(min(mNhc));
mNsz = squeeze(mean(Nsz,1));
mNsz(1:1+size(mNsz,1):end) = min(min(mNsz));
Lo=min(min(min(mNhc)),min(min(mNsz)));
Hi=max(max(max(mNhc)),max(max(mNsz)));

[fig, A, C, I] = plot_FNC(mNhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Static Functional Network Connectivity using Normalized Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);

[fig, A, C, I] = plot_FNC(mNsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Patient Static Functional Network Connectivity using Normalized Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);

% Next, we plot Normalized Mutual Information
mNhc = vec2mat(zscore(mat2vec(squeeze(mean(Nhc,1)))));
mNsz = vec2mat(zscore(mat2vec(squeeze(mean(Nsz,1)))));
mMhc = vec2mat(zscore(mat2vec(squeeze(mean(Mhc,1)))));
mMsz = vec2mat(zscore(mat2vec(squeeze(mean(Msz,1)))));
mRhc = vec2mat(zscore(mat2vec(squeeze(mean(Rhc,1)))));
mRsz = vec2mat(zscore(mat2vec(squeeze(mean(Rsz,1)))));

mRNhc = abs(mRhc) - mNhc;
mRNsz = abs(mRsz) - mNsz;
mRNhc(1:1+size(mRNhc,1):end) = min(min(mRNhc));
mRNsz(1:1+size(mRNsz,1):end) = min(min(mRNsz));
Lo=min(min(min(mRNhc)),min(min(mRNsz)));
Hi=max(max(max(mRNhc)),max(max(mRNsz)));

[fig, A, C, I] = plot_FNC(mRNhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
%title('Figure: Healthy Controls - Difference between Correlation and NMI.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
h = colorbar;
ylabel(h,'difference');
saveas(gcf,'zCorr-Nmi_HC.png');

[fig, A, C, I] = plot_FNC(mRNsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
%title('Figure: Patients - Difference of ZScores between Correlation and NMI.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
h = colorbar;
ylabel(h,'difference');
saveas(gcf,'zCorr-Nmi_SZ.png');

mRMhc = abs(mRhc) - mMhc;
mRMsz = abs(mRsz) - mMsz;
mRMhc(1:1+size(mRhc,1):end) = min(min(mRMhc));
mRMsz(1:1+size(mNsz,1):end) = min(min(mRMsz));
Lo=min(min(min(mRMhc)),min(min(mRMsz)));
Hi=max(max(max(mRMhc)),max(max(mRMsz)));

[fig, A, C, I] = plot_FNC(mRMhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
%title('Figure: Healthy Controls - Difference of ZScores between Correlation and MI.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
h = colorbar;
ylabel(h,'difference');
saveas(gcf,'zCorr-Mi_HC.png');

[fig, A, C, I] = plot_FNC(mRMsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
%title('Figure: Patients - Difference between Correlation and MI.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
h = colorbar;
ylabel(h,'difference');
saveas(gcf,'zCorr-Mi_SZ.png');

mMNhc = mMhc - mNhc;
mMNsz = mMsz - mNsz;
mMNhc(1:1+size(mMNhc,1):end) = max(max(mMNhc));
mMNsz(1:1+size(mMNsz,1):end) = max(max(mMNsz));
Lo=min(min(min(mMNhc)),min(min(mMNsz)));
Hi=max(max(max(mMNhc)),max(max(mMNsz)));

[fig, A, C, I] = plot_FNC(mMNhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
%title('Figure: Healthy Controls - Difference of ZScores between MI and NMI.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
h = colorbar;
ylabel(h,'difference');
saveas(gcf,'zMi-Nmi_HC.png');

[fig, A, C, I] = plot_FNC(mMNsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
%title('Figure: Patients - Difference of ZScores between MI and NMI.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
h = colorbar;
ylabel(h,'difference');
saveas(gcf,'zMi-Nmi_SZ.png');

%mPhc = squeeze(median(Phc,1));
%Lo=min(min(mPhc));
%Hi=max(max(mPhc));
%[fig, A, C, I] = plot_FNC(mPhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
%title 'Median Correlation PValues';
%%
close all;
