addpath(genpath('matcode/KGC'));
close all;

mR = squeeze(mean(R,1));
mR(1:1+size(mR,1):end) = min(min(mR));

figure();
imagesc(mR);
colormap jet;
colorbar;
title 'Mean Correlation';

%mR = squeeze(median(R,1));
%mR(1:1+size(mR,1):end) = min(min(mR));

%figure();
%imagesc(mR);
%colormap jet;
%colorbar;
%title 'Median Correlation';

mM = squeeze(mean(M,1));
mM(1:1+size(mM,1):end) = min(min(mM));

figure();
imagesc(mM);
colormap jet;
colorbar;
title 'Mean MI';

mN = squeeze(mean(N,1));
mN(1:1+size(mN,1):end) = min(min(mN));

figure();
imagesc(mN);
colormap jet;
colorbar;
title 'Mean NMI';

mP = squeeze(median(P,1));
%mP(mP>0.05)=-0.05;

figure();
imagesc(mP);
colormap jet;
colorbar;
title 'Median Correlation PValues';

mP = squeeze(mean(P,1));
%mP(mP>0.05)=-0.05;

figure();
imagesc(mP);
colormap jet;
colorbar;
title 'Mean Correlation PValues';

% figure();
% imagesc(F);
% colormap jet;
% colorbar;
% title 'Fishers Method PValues';


c1 = 14;
c1m = squeeze(median(P, 1));
p1= c1m(k1,:);
c2 = find(p1(2:end)==max(p1(2:end)))+1;
c2 = 18;


sc1 = squeeze(mean(hc(:,:,c1),1));
sc2 = squeeze(mean(hc(:,:,c2),1));

imax_lag = 1;
lmax_lag = 30;
maxint = imax_lag:lmax_lag;
firstYlag = 1;
alpha = 0.05;
%figure();
FF = zeros(lmax_lag-imax_lag,1);
c_v = zeros(lmax_lag-imax_lag,1);
Fprob = zeros(lmax_lag-imax_lag,1);

j = 1;
for max_lag = imax_lag:lmax_lag
    [ FF(j) , c_v(j),   Fprob(j), Fprob_cor, dAIC,  dBIC  , best_x_lag , best_y_lag    ] =granger_cause_1(sc2',sc1',alpha,max_lag,0,max_lag,0,firstYlag,'SC2','SC1',0,'.','Fprefix','Ptitle',0);
    j = j + 1;
end
c_v = c_v(find(Fprob==min(Fprob)));
FF = FF(find(Fprob==min(Fprob)));
max_lag = maxint(Fprob==min(Fprob));
Fprob = Fprob(find(Fprob==min(Fprob)));
fprintf('F statistic for Granger Causality is %d with random probability %f, and critical value %f. Components %d and %d with max_lag %f\n',FF,Fprob,c_v,c1,c2,max_lag);


figure();
b1 = mean(R(:,c1,c2));%sc2'\sc1';
y=b1*sc1;

fm=ksr(sc1,sc2);
ynl = fm.f;

hold on;
scatter(sc1,sc2,'.');
plot(sc1,y,'LineWidth',3);
plot(fm.x,fm.f,'LineWidth',3);
xlabel(sprintf('Component %d', c1));
ylabel(sprintf('Component %d', c2));
grid on;
grid minor;
legend({'Components', 'Linear Fit', 'NonLinear Fit'},'location','best');

figure();
hold on;
plot(sc1,'LineWidth',2);
plot(sc2,'LineWidth',2);
legend({sprintf('Component %d',c1),sprintf('Component %d',c2)},'location','best');
grid on;
grid minor;


c1R = squeeze(R(:,c1,c2));
c1P = squeeze(P(:,c1,c2));
c1M = squeeze(M(:,c1,c2));
c1N = squeeze(N(:,c1,c2));
c1L = length(c1R);
final_p_value = fisher_pvalue_meta_analysis(c1P);

fprintf('Mean Correlation Between %d and %d:  %f\n',c1,c2, mean(c1R));
fprintf('Median Correlation Between %d and %d:  %f\n', c1,c2,median(c1R));
fprintf('Mean MI Between %d and %d:  %f\n',c1,c2, mean(c1M));
fprintf('Median MI Between %d and %d:  %f\n', c1,c2,median(c1M));
fprintf('Mean NMI Between %d and %d:  %f\n',c1,c2, mean(c1N));
fprintf('Median NMI Between %d and %d:  %f\n', c1,c2,median(c1N));
fprintf('Mean P-Value Between %d and %d:  %f\n',c1,c2, mean(c1P));
fprintf('Median P-Value Between %d and %d:  %f\n', c1,c2,median(c1P));
fprintf('Fisher P-Value Between %d and %d:  %f\n', c1,c2,final_p_value);
fprintf('Number of |Correlations above 0.15| Between %d and %d:  %d/%d\n', c1,c2, sum(abs(c1R)>0.15),c1L);
fprintf('Number of |Correlations below 0.15| Between %d and %d:  %d/%d\n', c1,c2, sum(abs(c1R)<0.15),c1L);

fprintf('Number of Significant Comparisons Between %d and %d:  %d/%d\n', c1,c2, sum(c1P<0.05),c1L);
fprintf('Number of Insignificant Comparisons Between %d and %d:  %d/%d\n', c1,c2, sum(c1P>0.05),c1L);

figure();
h=histfit(c1R,20,'kernel');
set(h(2),'color','k');
hold on;
v1=vline(mean(c1R),'r-');
set(v1,'LineWidth',3);
v2=vline(median(c1R),'b');
set(v2,'LineWidth',3);title Correlations;
grid on;
grid minor;
plot(NaN,'r','LineWidth',3);
plot(NaN,'b','LineWidth',3);
legend({'Frequency','Fit','Mean','Median'},'location','best');

figure();
h=histfit(c1M,20,'kernel');
set(h(2),'color','k');
hold on;
v1=vline(mean(c1M),'r-');
set(v1,'LineWidth',3);
v2=vline(median(c1M),'b');
set(v2,'LineWidth',3);
title MI;
grid on;
grid minor;
plot(NaN,'r','LineWidth',3);
plot(NaN,'b','LineWidth',3);
legend({'Frequency','Fit','Mean','Median'},'location','best');

figure();
h=histfit(c1N,20,'kernel');
set(h(2),'color','k');
hold on;
v1=vline(mean(c1N),'r-');
set(v1,'LineWidth',3);
v2=vline(median(c1N),'b');
set(v2,'LineWidth',3);
title NMI;
grid on;
grid minor;
plot(NaN,'r','LineWidth',3);
plot(NaN,'b','LineWidth',3);
legend({'Frequency','Fit','Mean','Median'},'location','best');


figure();
h=histfit(c1P,20,'kernel');
hold on;
set(h(2),'color','k');
v1=vline(mean(c1P),'r-');
set(v1,'LineWidth',3);
v2=vline(median(c1P),'b');
set(v2,'LineWidth',3);
title Pvalues;
grid on;
grid minor;
plot(NaN,'r','LineWidth',3);
plot(NaN,'b','LineWidth',3);
legend({'Frequency','Fit','Mean','Median'},'location','best');

autoArrangeFigures;
