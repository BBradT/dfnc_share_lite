% PARAMETERS
SAVE_RESULTS = 1;   			% Save results after analysis
USE_TIMESTAMP = 0;  			% Use a timestamp to avoid overwriting old results

% INITIALIZE RUNTIME VARS
if USE_TIMESTAMP
    cl = clock;
    runtime = fix(cl);
    runtime_str = sprintf('%d_%d_%d_%d_%d_%d',runtime(:));
else
    runtime_str = '';
end
disp(sprintf('*********************\nStarting ICA analysis\n*********************'));
if USE_TIMESTAMP
    disp(sprintf('\tTimestamp: %s', runtime_str));
end


% Run InfoMax ICA
disp('Running InfoMax ICA');
[dum, W, sph] = evalc('icatb_runica((X*V)'');');

% Reconstruct SMs
disp('Estimating Spatial Maps');
SM = pinv(W*sph*(X*V)');

% Reconstruct TCs
disp('Estimating Timecourses');
TC = pinv(SM)*Xold;
TC = reshape(TC, NC, mode(TIME), NUM_SUBJ);
TC = permute(TC, [3, 2, 1]);

if SAVE_RESULTS
    if ~exist(RESULT_DIR, 'dir')
        mkdir(RESULT_DIR);
    end
    disp(sprintf('Saving ica_results_%s.mat',runtime_str));
    save(sprintf('%s/ica_results_%s.mat', RESULT_DIR, runtime_str), 'W','V','SM','TC','sph');
end

% clearvars -except SM TC
% fprintf('\n****DONE****\n\nResulting Variables:\n');
% whos
