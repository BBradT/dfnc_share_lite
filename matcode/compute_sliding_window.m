function c = compute_sliding_window(nT, win_alpha, wsize)
%% Compute sliding window
%
%  usage: c = compute_sliding_window(nT, win_alpha, wsize)
%

nT1 = nT;
if mod(nT, 2) ~= 0
    nT = nT + 1;
end

m = nT/2;
w = round(wsize/2);
%if (strcmpi(win_type, 'tukey'))
%    gw = icatb_tukeywin(nT, win_alpha);
%else
gw = gaussianwindow(nT, m, win_alpha);
%end
b = zeros(nT, 1);  b((m -w + 1):(m+w)) = 1;
c = conv(gw, b); c = c/max(c); c = c(m+1:end-m+1);
c = c(1:nT1);
