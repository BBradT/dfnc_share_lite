function DVARS = compute_DVARS(TC)

% TC should be [nT x nC] or [nT x nV](masked)

DVARS = sqrt(mean(diff(TC).^2,2));