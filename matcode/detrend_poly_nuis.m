function y = detrend_poly_nuis(x, p, m) 

r = size(x,1); 
b1 = ((1 : r)' * ones (1, p + 1)) .^ (ones (r, 1) * (0 : p));  % build the regressors
if ~exist('m','var')
  b= b1;
else
    
b = [b1 m];
end
y = x - b * (b\x); % remove the best fit
