function TCout = uint8_preservable(TCin, scale)
    if ~exist('scale','var')
        scale = 10;
    end
    A=min(TCin(~~TCin));
    if isempty(A)
        A = min(TCin);
    end
    TCout = scale*(TCin+abs(A));
end
