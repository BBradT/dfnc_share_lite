fprintf(['This environment contains all necessary MATLAB scripts for running\n'...
         '    dynamic functional connectivity (dFNC) analysis.\n'...
         '    see the help below for instructions on the usage of included functions.\n\n']);

code_directory = dir('./matcode/');

for cd_i = 1:length(code_directory)
    if strfind(code_directory(cd_i).name, '.m') && ~strfind(code_directory(cd_i).name,'helpdfnc')
        fprintf(code_directory(cd_i).name);
        fprintf('\n');
        help(code_directory(cd_i).name);
        fprintf('\n');
    end
end

clearvars code_directory cd_i
