disp(sprintf('*********************\nStarting dFNC\n*********************'));

close all;
NUM_SUBJ = 314;				% Number of Subjects to Load
% NUM_SUBJ = 204;  			% Maximum number of available COBRE Subjects
DATA_FOLDER = './data/fbirn/';		% Location of Data
REF_FILE = './data/parameters/fbirn_reference.mat';		% Location of Data
MASK_FILE = './data/parameters/fbirn_mask.mat';
RESULT_DIR = './results/';		% Location of Results
RESTRICT = 0;				% Restrict components by matching with known GT
NC = 100;				% Number of ICs to estimate
SUBJ_PC = 120;				% Number of Subject-Level PCs
NUM_STATES = 5;

load /export/mialab/users/eswar/fbirn_p3/results_C100_vn/TC/fbirnp3_rest_C100_ica_TC_scrubbed_filt_RSN.mat
TC_rsn_filt = permute(TC_rsn_filt,[2,3,1]);

[L, RSN, Art, MOD] = comp_labels_fb_C100_vn_FINAL;
%% STEP 4 : Run dFNC to compute covariance matrices
dfnc = rundfnc(TC_rsn_filt);
dfnc_mi = rundfnc(TC_rsn_filt,'metric','mi');
dfnc_nmi = rundfnc(TC_rsn_filt,'metric','nmi');

%% STEP 5 : Run KMeans clustering 
[IDXall, Call, sumD_all, Dall] = run_kmeans(dfnc, NUM_STATES);

%% PLOT FINAL STATES
figs = cell(1,NUM_STATES);
for i = 1:NUM_STATES
    [figs{i},A,C,I] = plot_FNC(Call(i,:),[-0.04,0.04],L,1:47,'w','correlation',MOD,'jet');
    title(sprintf('Centroid %d',i));
end

figSize = [21, 29];            % [width, height]
figUnits = 'Centimeters';

if ~exist('./results/', 'dir')
    mkdir('./results/');
end

for f = 1:numel(figs)
      fig = figs{f};

      % Resize the figure
      set(fig, 'Units', figUnits);
      pos = get(fig, 'Position');
      pos = [pos(1), pos(4)+figSize(2), pos(3)+figSize(1), pos(4)];
      set(fig, 'Position', pos);

      % Output the figure
      filename = sprintf('./results/State_%02d.pdf', f);
      print( fig, '-dpdf', filename ,'-bestfit');

end

save('results/dfnc_results.mat')
