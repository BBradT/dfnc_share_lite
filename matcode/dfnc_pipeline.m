disp(sprintf('*********************\nStarting dFNC\n*********************'));

close all;
NUM_SUBJ = 314;				% Number of Subjects to Load
% NUM_SUBJ = 204;  			% Maximum number of available COBRE Subjects
DATA_FOLDER = './data/fbirn/';		% Location of Data
REF_FILE = './data/parameters/fbirn_reference.mat';		% Location of Data
MASK_FILE = './data/parameters/fbirn_mask.mat';
RESULT_DIR = './results/';		% Location of Results
RESTRICT = 0;				% Restrict components by matching with known GT
NC = 100;				% Number of ICs to estimate
SUBJ_PC = 120;				% Number of Subject-Level PCs
NUM_STATES = 5;

% Step 0 - Centering and PCA Preprocessing
datasets = load_data(DATA_FOLDER, NUM_SUBJ);
mask = load(MASK_FILE);
for i = 1:length(datasets)
    datasets{i} = apply_mask(datasets{i}, 'mask', mask.mask);
end
Xold = [datasets{:}];

% Assuming Voxel x Time matrices
[VOXEL, TIME] = cellfun(@size,datasets,'UniformOutput',1);


disp('Centering Data and Performing Subject-Level PCA');
for i = 1:length(datasets)
    datasets{i} = datasets{i} - repmat(mean(datasets{i},2), 1, size(datasets{i},2));
    [dum, datasets{i}, ~] = evalc('icatb_calculate_pca(datasets{i}, SUBJ_PC, ''whiten'', 1);');
end

% Concatenate in the Temporal Dimension
X = [datasets{:}];  % X is Voxel x Time

% Run Groupl-Level PCA
disp('Group-Level PCA Preprocessing');
[dum, V] = evalc('icatb_calculate_pca(X, NC, ''whiten'', 0);');  % V should be Voxel x NC

%% STEP 1 : Run InfoMax ICA
runica;

%% STEP 2 : Match Components to Reference
oTC = TC;
[new,agg,TC,L,MOD] = get_component_matches(SM, REF_FILE, RESTRICT, oTC);

%% STEP 3 (fBirn data only) : additional processing of TimeCourses
process_tc;

%% STEP 4 : Run dFNC to compute covariance matrices
dfnc = runsfnc(TCfilt);

%% STEP 5 : Run KMeans clustering 
[IDXall, Call, sumD_all, Dall] = run_kmeans(dfnc, NUM_STATES);

%% PLOT FINAL STATES
figs = cell(1,NUM_STATES);
for i = 1:NUM_STATES
    [figs{i},A,C,I] = plot_FNC(Call(i,:),[-0.04,0.04],L,1:47,'w','correlation',MOD,'jet');
    title(sprintf('Centroid %d',i));
end

figSize = [21, 29];            % [width, height]
figUnits = 'Centimeters';

if ~exist('./results/', 'dir')
    mkdir('./results/');
end

for f = 1:numel(figs)
      fig = figs{f};

      % Resize the figure
      set(fig, 'Units', figUnits);
      pos = get(fig, 'Position');
      pos = [pos(1), pos(4)+figSize(2), pos(3)+figSize(1), pos(4)];
      set(fig, 'Position', pos);

      % Output the figure
      filename = sprintf('./results/State_%02d.pdf', f);
      print( fig, '-dpdf', filename ,'-bestfit');

end

