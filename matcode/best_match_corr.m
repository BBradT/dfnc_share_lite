function [RMAT, mINFO, Rmax, mSign]  = best_match_corr(refmaps, newmaps)
% Return best match of spatial map indices using correlation
%
%   usage: [RMAT, mINFO, Rmax, mSign] = best_match_corr(reference, new)
%		RMAT = r measure matrix
%		mINFO = matching indices
%		Rmax = maximum r measure
%		mSign = sign of match
%
%
RMAT = corr(refmaps, newmaps).^2;
nC = size(newmaps,2);
mINFO = zeros(nC,2);
Rmax = zeros(nC,1);
mSign = zeros(nC,1);
[mv, refind] = max(RMAT);

%fprintf('Ref\t\t New\t\t R^2\t\t Sign\n')
for ii = 1:nC
    mINFO(ii,:) = [refind(ii), ii];
    Rmax(ii) = mv(ii);
    mSign(ii) = sign(corr(refmaps(:,refind(ii)), newmaps(:,ii)));
    %fprintf('%d\t\t %d\t\t %0.4f\t\t %d\n',refind(ii), ii, mv(ii),  mSign(ii));
end
