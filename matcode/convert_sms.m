load data/labels.mat

cd /export/mialab/users/eswar/fbirn_p3/results_C100_vn/SM

all = zeros(314,60303,47);

for i = RSN
       fn = num2str(i,'fbirnp3_rest_SM_comp%03.f.mat');
       disp(fn);
       load(fn);
       all(:,:,i) = SM;
end

subject_sm = cell(1,314);

disp('Partitioning!');

parfor i = 1:314
    disp(i);
    subject_sm{i} = squeeze(all(i,:,:))';
end

save('/export/mialab/users/bbaker/projects/dfnc_share/data/fbirn_spatial_maps.mat','subject_sm','-v7.3');
disp('done!');
