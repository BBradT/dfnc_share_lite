function [F,A,C,I] = plot_FNC(FNC, CLIM, LABEL, RSN_I, B, T,MOD,cm)
% [F,A,C,I] = plot_FNC(FNC, CLIM, LABEL, RSN_I, B, T,MOD,cm)
% FNC is a matrix [Nc x Nc] or vector (of lower triangle values) [nchoosek(Nc,2) x 1]
% CLIM (data limits for plotting) ex: [-0.6 0.6]
% LABEL: N x 1 cell with labels for N ICs (N is the total number of components in the decomposition of which Nc are good components)
% RSN_I: Nc x 1 vector of indices of good components 
% Figure background color: default: 'w' (white)
% T: type of data correlation or p-values or -log10(p-values) etc. Default: 'correlation'
% MOD: a vector of K x 1 with number/count of components in each sub-domain (sum(MOD) = Nc)
% cm: colormap. Default : jet
% All arguments except FNC are optional
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MODULES_NAMES = {'SC','AUD','VIS','SM','CC','DM','CB'};

%if nargin < 7 || ~isvarname('MOD')
%    [aa ab ac MOD] = comp_labels_fb_C100_vn;
%end
% define the size of each "MODULE"
%MOD = [5, 2, 6, 10, 8, 7, 7, 1];
cMOD = cumsum(MOD);
if isvector(FNC)
    FNC = vec2mat(FNC, 1);
else
    % make sure Nans are along the diagonal
    tmp = eye(length(FNC));
    FNC(tmp==1) = NaN;
  %  FNC = vec2mat(mat2vec(FNC),1);
end


if ~exist('CLIM', 'var') || isempty(CLIM)
    if any(FNC(:)<0)
         CLIM = [-max(abs(FNC(:))), max(abs(FNC(:)))];
    else 
        temp = mat2vec(FNC);
        CLIM = [min(temp(:)), max(temp(:))];
    end
end


if ~exist('L', 'var') || isempty(L)
   L = cell(length(FNC),1);
   for ii=1:length(FNC)
        L{ii} = ['IC' num2str(ii)];
   end
end

if ~exist('RSN_I', 'var') || isempty(RSN_I)
     RSN_I = 1:length(FNC);
end

if ~exist('T', 'var') || isempty(T)
    T = 'correlation';
end

if ~exist('B', 'var') || isempty(B)
   F = figure('Color', 'w', 'visible', 'on');
else
   F = figure('Color',B, 'visible','on');
end
set(F,'Position', [0, 0.0, 1098, 1098]);
if ~exist('cm', 'var') || isempty(cm)
   CM = 'jet';
else
   CM = cm;
end
%load('coldhot_white_128');
%   cm = CM;
%imagesc(FNC, CLIM);
simtb_pcolor(1:length(RSN_I), 1:length(RSN_I), FNC');
set(gca, 'clim', CLIM); axis ij;
colormap(CM);
if exist('MOD', 'var')
    cMOD = cumsum(MOD);
    c = get(gca, 'Children');
    set(c(find(strcmp(get(c, 'Type'),'line'))), 'Color', [0.85 0.85 0.85]);
    set(c(length(RSN_I)-cMOD+1), 'Color', 'k')
    set(c(2*(length(RSN_I)+1)-cMOD), 'Color', 'k')
    uistack(c(2*(length(RSN_I)+1)-cMOD), 'top')
end
A = gca;
I = c(find(strcmp(get(c, 'Type'),'image')));
C = colorbar; 
set(get(C, 'YLabel'), 'String', T)
axis square

%% labeling
if exist('LABEL', 'var') && ~isempty(LABEL)
    
    set(A, 'YTick', 1:length(RSN_I), 'YTickLabel', LABEL(RSN_I), 'TickDir', 'out', 'XTick', 1:length(RSN_I),'XTickLabel',[]);
    b = get(A, 'XTick');
    c=get(A,'YTick');
    ypos = c(end) +1.25;
    th=text(1:length(b),repmat(ypos,length(b),1), LABEL(RSN_I),'HorizontalAlignment','right','rotation',90);
end

for mi = 1:length(MOD)
   thh = text(cMOD(mi)-MOD(mi)/2-0.1,0,MODULES_NAMES{mi});
   set(thh,'Color','k','FontSize',12);
end

if exist('RSN_I', 'var') && ~isempty(RSN_I)
    hold on;
    
    for ii = 1:length(RSN_I);
        T =text(ii,ii,num2str(RSN_I(ii)));
        set(T, 'Color', 'w', 'HorizontalAlignment', 'Center');
    end
end

