function [new_match, ref_match, varargout] = get_component_matches(new_maps, ref_file, restrict_labels, tc)
% match new estimated spatial maps with fbirn reference

if ~exist('ref_file', 'var')
    ref_file = 'data/parameters/cobre_reference.mat';
end

if ~exist('restrict_labels', 'var')
    restrict_labels = 0;
end
fprintf('Loading reference file %s\n', ref_file)
load(ref_file);

fprintf('Doing greedy match\n')
[RMAT, mINFO, Rmax, mSign]  = best_greedy_match_corr(ref, new_maps);


if restrict_labels
[L, RSN, Art, modules] = comp_labels_fb_C100_vn_FINAL;
L = L(RSN);
idxs = arrayfun(@(x) find(mINFO(:, 1) == x), RSN,'UniformOutput',1);
else
idxs = 1:size(mINFO, 1);
end
iref =  mINFO(idxs, 1);
inew = mINFO(idxs, 2);
new_match = new_maps(:,inew);
ref_match = ref(:,iref);

if exist('tc','var')
    varargout{1} = tc(:,:,inew);
end

if restrict_labels
varargout{2} = L;
varargout{3} = modules;
varargout{4} = RSN;
varargout{5} = Art;
end
varargout{6} = RMAT;
varargout{7} = mINFO;
varargout{8} = Rmax;
varargout{9} = mSign;
varargout{10} = ref;

end
