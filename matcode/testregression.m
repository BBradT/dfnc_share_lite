load data/fbirn_tc
load data/parameters/DEMO_ICA_fBIRNp3_New

%% init
NC=47;
sz=TC(DEMO.full(:,3)==1,:,:);
hc=TC(DEMO.full(:,3)==0,:,:);
close all;
Lhc = size(hc,1);
Lsz = size(sz,1);
RRRhc = zeros(Lhc, NC, NC,159);
RRRsz = zeros(Lsz, NC, NC,159);
RRhc = zeros(Lhc, NC, NC);
RRsz = zeros(Lsz, NC, NC);
[L, RSN, Art, MOD] = comp_labels_fb_C100_vn_FINAL;
L = L(RSN);
parfor i=1:Lhc
    for c1=1:NC
        sc1 = squeeze(hc(i,:,c1));
        for c2=1:NC
            fprintf('HC subject %d, Comparing component %d with %d\n',i,c1,c2);
            sc2 = squeeze(hc(i,:,c2));
            b1 = sc2'\sc1';
            y=b1*sc1;
            RRRhc(i,c1,c2,:) = sc2-y;
            RRhc(i,c1,c2) = norm(sc2 - y);
        end
    end
end
parfor i=1:Lsz
    for c1=1:NC
        sz1 = squeeze(sz(i,:,c1));
        for c2=1:NC
            fprintf('SZ subject %d, Comparing component %d with %d\n',i,c1,c2);
            sz2 = squeeze(sz(i,:,c2));
            b1 = sz2'\sz1';
            y=b1*sz1;
            RRRsz(i,c1,c2,:) = sz2-y;
            RRsz(i,c1,c2) = norm(sz2 - y);
        end
    end
end

parfor c1=1:NC
    for c2=1:NC
        fprintf('Residuals - Comparing component %d with %d\n',c1,c2);
        componentDir = sprintf('results/NL/NL-%dVS%d/',c1,c2);
        if ~exist(componentDir,'dir')        
            mkdir(componentDir);
        end
        if c1 ~= 14
            f=figure('visible','off');
        else
            f= figure;
        end
        subplot(1,2,1);
        scatter(squeeze(mean(hc(:,:,c1))),squeeze(mean(RRRhc(:,c1,c2,:),1)),512,'.');
        axis square;
        title('HC');
        xlabel('$x$','interpreter','latex');
        ylabel('$\hat{y} - y$','interpreter','latex');
        set(gca,'FontSize',18);
        grid on;
        grid minor;
        subplot(1,2,2);
        scatter(squeeze(mean(sz(:,:,c1))),squeeze(mean(RRRsz(:,c1,c2,:),1)),512,'.');
        axis square;
        title('SZ');
        xlabel('$x$','interpreter','latex');
        ylabel('$\hat{y} - y$','interpreter','latex');
        set(gca,'FontSize',18);
        set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
        grid on;
        grid minor;
        suptitle(sprintf('%d - %d',c1,c2));
        saveas(gcf,[componentDir sprintf('residual_%dVS%d.png',c1,c2)]);
        close all;
    end
end

%%
exRRhc = squeeze(RRhc(:,14,25));

%%
close all;
mRRhc = squeeze(mean(RRhc,1));
mRRsz = squeeze(mean(RRsz, 1));
mRRhc(1:1+size(RRhc,1):end) = min(min(mat2vec(mRRhc)));
mRRsz(1:1+size(RRsz,1):end) = min(min(mat2vec(mRRsz)));
Lo=min(min(min(mRRhc)),min(min(mRRsz)));
Hi=max(max(max(mRRhc)),max(max(mRRsz)));
[fig1, A, C, I] = plot_FNC(mRRhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
C = colorbar; 
set(get(C, 'YLabel'), 'String', '$||\beta x - y||_2$', 'Interpreter', 'Latex', 'FontSize',18);
saveas(gcf,'Res_HC.png');

[fig2, A, C, I] = plot_FNC(mRRsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
C = colorbar; 
set(get(C, 'YLabel'), 'String', '$||\beta x - y||_2$', 'Interpreter', 'Latex', 'FontSize',18);
saveas(gcf,'Res_SZ.png');
