load data/fbirn_tc
load data/parameters/DEMO_ICA_fBIRNp3_New

%% init
TC=uint8_preservable(TC, 10);
sz=TC(DEMO.full(:,3)==1,:,:);
hc=TC(DEMO.full(:,3)==0,:,:);


Nsz = size(sz,1);
Nhc = size(hc,1);
T = size(sz,2);
NC = size(sz,3);

sz_je = zeros(Nsz,NC,NC);

for i = 1:Nsz
   for k = 1:NC
       s1 = uint8(squeeze(sz(i,:,k)));
       for k2 = 1:NC
           s2 = uint8(squeeze(sz(i,:,k2)));
           [mi,nmi,e1,e2,sz_je(i,k,k2)] = RMI(s1,s2);
       end
   end
   fprintf('Subject %d done\n',i);
end
%%
close all;
M = squeeze(mean(sz_je,1));
M(1:1+size(M,1):end) = max(max(M));
figure();
imagesc(M);
colormap jet;
colorbar();

