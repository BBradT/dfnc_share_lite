close all;
load data/fbirn_tc_separated.mat
tc1 = squeeze(hc(1,:,14));
%tc1 = sin(linspace(-pi,pi,100))+1;
tcf1 = uint8_preservable(tc1,10);

params = {
    'LineWidth', 2
    };

bs = 1:1:10;
cmap = jet(length(bs))/1.25;
r = 1;
correlations = zeros(r,length(bs));
mis = zeros(r,length(bs));
nmis = zeros(r,length(bs));
legs = cell(1,length(bs));
figure;
hold on;
for i = 1:length(bs)
    b = bs(i);
    yl = tc1 * b + b;

    TC = uint8_preservable([tc1;yl],10);
    tcf1 = TC(1,:);
    y = TC(2,:);
    scatter(uint8(tcf1),uint8(y),20,cmap(i,:),params{:});
    correlations(i) = corr(y',tcf1');
    legs{i} = sprintf('$b$=%d',b);
    [mis(i), nmis(i)] = RMI(uint8(y),uint8(tfc1));
    bhat = y/tfc1;
    yhat = tfc1 * bhat;
end
    
xlabel('$x$','interpreter','latex');
ylabel('$bx+b$','interpreter','latex');
set(gca,'FontSize',18);
legend(legs,'fontsize',12,'location','best','interpreter','latex');
grid on;
grid minor;
saveas(gcf,'results/linear_relations.png');

figure;
plot(bs,correlations,'r-o',params{:});
hold on;
plot(bs,mis,'b-o',params{:});
plot(bs,nmis,'g-o',params{:});
set(gca,'FontSize',18);
ylim([0,max(mis)]);
grid on;
grid minor;
xlim([min(bs), max(bs)]);
ylabel('$d(x,bx+b)$','interpreter','latex');
xlabel('$b$','interpreter','latex');
legend({'Correlation','MI','NMI'},'location','best','fontsize',12);
saveas(gcf,'results/linear_measures.png');

% Nonlinear

bs = 1:1:10;
cmap = jet(length(bs))/1.25;
r = 1;
correlations = zeros(r,length(bs));
mis = zeros(r,length(bs));
nmis = zeros(r,length(bs));
legs = cell(1,length(bs));
figure;
hold on;
colormap colorcube;
for i = 1:length(bs)
    b = bs(i);
    yl = b*tc1.^2 + tc1 + b;
    TC = uint8_preservable([tc1;yl],10);
    tcf1 = TC(1,:);
    y = TC(2,:);
    legs{i} = sprintf('$b$=%d',b);
    scatter(uint8(tcf1),uint8(y),20,cmap(i,:),params{:});
    correlations(i) = corr(y',tcf1');
    [mis(i), nmis(i)] = RMI(uint8(y),uint8(tfc1));
end

xlabel('$x$','interpreter','latex');
ylabel('$bx^2+x+b$','interpreter','latex');
set(gca,'FontSize',18);
legend(legs,'fontsize',12,'location','best','interpreter','latex');
grid on;
grid minor;
saveas(gcf,'results/nonlinear_relations.png');

figure;
plot(bs,correlations,'r-o',params{:});
hold on;
plot(bs,mis,'b-o',params{:});
plot(bs,nmis,'g-o',params{:});
set(gca,'FontSize',18);
ylim([0,max(mis)]);
grid on;
grid minor;
xlim([min(bs), max(bs)]);
xlabel('$b$','interpreter','latex');
ylabel('$d(x,bx^2+x+b)$','interpreter','latex');
legend({'Correlation','MI','NMI'},'location','best','fontsize',12);
saveas(gcf,'results/nonlinear_measures.png');


% Cubed

bs = 1:1:10;
cmap = jet(length(bs))/1.25;
r = 1;
correlations = zeros(r,length(bs));
mis = zeros(r,length(bs));
nmis = zeros(r,length(bs));
legs = cell(1,length(bs));
figure;
hold on;
colormap colorcube;
for i = 1:length(bs)
    b = bs(i);
    yl = b*tc1.^(2) + b;
    TC = uint8_preservable([tc1;yl],10);
    tcf1 = TC(1,:);
    y = TC(2,:);
    legs{i} = sprintf('$b$=%d',b);
    scatter(uint8(tcf1),uint8(y),20,cmap(i,:),params{:});
    correlations(i) = corr(y',tcf1');
    [mis(i), nmis(i)] = RMI(uint8(y),uint8(tfc1));
end

xlabel('$x$','interpreter','latex');
ylabel('$bx^2+b$','interpreter','latex');
set(gca,'FontSize',18);
legend(legs,'fontsize',12,'location','best','interpreter','latex');
grid on;
grid minor;
saveas(gcf,'results/nonlinear2_relations.png');

figure;
plot(bs,correlations,'r-o',params{:});
hold on;
plot(bs,mis,'b-o',params{:});
plot(bs,nmis,'g-o',params{:});
set(gca,'FontSize',18);
ylim([min(correlations),max(mis)]);
grid on;
grid minor;
xlim([min(bs), max(bs)]);
xlabel('$b$','interpreter','latex');
ylabel('$d(x,bx^2+b)$','interpreter','latex');
legend({'Correlation','MI','NMI'},'location','best','fontsize',12);
saveas(gcf,'results/nonlinear2_measures.png');

% Sinusoid

bs = 1:1:10;
cmap = jet(length(bs))/1.25;
r = 1;
correlations = zeros(r,length(bs));
mis = zeros(r,length(bs));
nmis = zeros(r,length(bs));
legs = cell(1,length(bs));
figure;
hold on;
colormap colorcube;
for i = 1:length(bs)
    b = bs(i);
    yl = b*sin(tc1) + tc1 + b;
    TC = uint8_preservable([tc1;yl],10);
    tcf1 = TC(1,:);
    y = TC(2,:);
    legs{i} = sprintf('$b$=%d',b);
    scatter(uint8(tcf1),uint8(y),20,cmap(i,:),params{:});
    correlations(i) = corr(y',tcf1');
    [mis(i), nmis(i)] = RMI(uint8(y),uint8(tfc1));
end

xlabel('$x$','interpreter','latex');
ylabel('$b\cdot sin(x)+x+b$','interpreter','latex');
set(gca,'FontSize',18);
legend(legs,'fontsize',12,'location','best','interpreter','latex');
grid on;
grid minor;
saveas(gcf,'results/sinusoid_relations.png');

figure;
plot(bs,correlations,'r-o',params{:});
hold on;
plot(bs,mis,'b-o',params{:});
plot(bs,nmis,'g-o',params{:});
set(gca,'FontSize',18);
ylim([min(correlations),max(mis)]);
grid on;
grid minor;
xlim([min(bs), max(bs)]);
xlabel('$b$','interpreter','latex');
ylabel('$d(x,b\cdot sin(x)+x+b)$','interpreter','latex');
legend({'Correlation','MI','NMI'},'location','best','fontsize',12);
saveas(gcf,'results/sinusoid_measures.png');

tilefigs;

