function result= JIDT_MI_kernel(source,destination)
    % result= JIDT_MI(signal1,signal2) will return the MI between signal1 and signal using
    %kernel estimator
    % Add JIDT jar library to the path, and disable warnings that it's already there:
    warning('off','MATLAB:Java:DuplicateClass');
    javaaddpath('matcode/infodynamics-dist-1.5/infodynamics.jar');
    % Add utilities to the path
    addpath('matcode/infodynamics-dist-1.5/demos/octave');

    % 0. Load/prepare the data:
%     data = load('G:\BC lab\dfnc_share-master\dfnc_share-master\matcode\infodynamics-dist-1.5\demos\data\2coupledRandomCols-1.txt');
    % Column indices start from 1 in Matlab:
    %source = octaveToJavaDoubleArray(data(:,1));
    %destination = octaveToJavaDoubleArray(data(:,2));

    % 1. Construct the calculator:
    calc = javaObject('infodynamics.measures.continuous.kernel.MutualInfoCalculatorMultiVariateKernel');
    % 2. Set any properties to non-default values:
    calc.setProperty('NORMALISE', 'false');
    % 3. Initialise the calculator for (re-)use:    
    calc.initialise();
    % 4. Supply the sample data:
    calc.setObservations(source, destination);
    % 5. Compute the estimate:
    result = calc.computeAverageLocalOfObservations();

    
%     fprintf('MI_Kernel(col_0 -> col_1) = %.4f bits\n', ...
%     result);

end