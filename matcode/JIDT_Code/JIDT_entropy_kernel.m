function result=JIDT_entropy_kernel(variable)
% Add JIDT jar library to the path, and disable warnings that it's already there:
warning('off','MATLAB:Java:DuplicateClass');
javaaddpath('matcode/infodynamics-dist-1.5/infodynamics.jar');
% Add utilities to the path
addpath('matcode/infodynamics-dist-1.5/demos/octave');

% 1. Construct the calculator:
calc = javaObject('infodynamics.measures.continuous.kernel.EntropyCalculatorKernel');
% 2. Set any properties to non-default values:
% No properties were set to non-default values
% 3. Initialise the calculator for (re-)use:
calc.setProperty('NORMALISE', 'false');
calc.initialise();
% 4. Supply the sample data:
calc.setObservations(variable);
% 5. Compute the estimate:
result = calc.computeAverageLocalOfObservations();

%fprintf('H_Kernel(col_0) = %.4f bits\n', ...
%	result);
end