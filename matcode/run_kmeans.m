function [IDXall, Call, sumD_all, Dall] = run_kmeans(dFNC, num_clusters)

% dFNC: subjects*windows*network-pairs
dmethod = 'correlation';
kmeans_num_replicates_initial = 100;
kmeans_num_replicates_final = 1;
kmeans_max_iter = 1024; 

size_dFNC = size(dFNC);
% Find Extremas in surrogate for initializing clustering   
SP = cell(1,size_dFNC(1));
for jj = 1:size_dFNC(1)     
    % Do initial clustering to get starting point (Maximally varying extrema points)
    temp =  squeeze(dFNC(jj, :, :));
    DEV = std(temp, [], 2);
    [~, imax, ~, ~] = extrema(DEV);
    pIND = sort(imax);        
    SP{jj} = temp(pIND, :)';
end
        
dFNCflat = reshape(dFNC, size_dFNC(1)*size_dFNC(2), size_dFNC(3));
    
% Concatenate Extrema Points 
SPflat = cell2mat(SP);   

% Cluster concatenated Extrema Points
[~, Cp, ~, ~] = kmeans(SPflat', num_clusters, ... 
                               'distance', dmethod, ...
                               'Replicates',  kmeans_num_replicates_initial, ...
                               'MaxIter', kmeans_max_iter, ... 
                               'Display', 'iter', 'empty', 'drop');

% Use above centroids as a starting point (Cp) to cluster all the data
[IDXall, Call, sumD_all, Dall] = kmeans(dFNCflat, num_clusters, ...
                                       'distance', dmethod, ...
                                       'Replicates', kmeans_num_replicates_final, ...
                                       'MaxIter', kmeans_max_iter, ...
                                       'Display', 'iter', ...                                           
                                       'empty', 'drop', ...
                                       'Start', Cp);
