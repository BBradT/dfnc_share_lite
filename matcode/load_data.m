function [datasets] = load_data(directory, ns, RESHAPE, prefix)
% Load .mat datasets from a directory
% 
%  datasets = load_data(directory);
%  ns - number of subjects
%  RESHAPE - flag to reshape into 3d images
%

RESHAPE = 1

if ~exist('prefix', 'var')
    prefix = 'vsdwa';
end

d = dir(directory);
if ~exist('ns', 'var')
     ns = length(d);   
end
datasets = cell(1, length(d));

disp(sprintf('Loading %d datasets from %s', ns, directory));

c = 1;
for i = 1:length(d)
   if c > ns
       break;
   end
   [fp,fname,ext] = fileparts(d(i).name);
   if strfind(fname, prefix) & strcmp(ext,'.nii')
       disp(sprintf('\tLoading %s\t\t %d/%d', fname,c,ns));
       nii = load_nii([directory  fname ext]);
       si = size(nii.img);
       datasets{i} = reshape(nii.img, prod(si(1:3)),si(4));
       c = c + 1; 
   end
end
datasets = datasets(~cellfun('isempty', datasets));
