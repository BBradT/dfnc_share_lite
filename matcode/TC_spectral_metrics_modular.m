function TC_spectral_metrics_modular(TCall,TR, prefix,outputdir,motpar_files)
% 10.6.2010 EAA
% script to compute spectral metrics from component TCs
% assumes that SPM and icatb (GIFT) scripts are on your path 
%
% 1. Loads in TCs from GIFT output 
% 2. Estimates power spectrum using multi-taper estimation (chronux toolbox;  alternatively can use FFT or PWELCH)
% 3. Computes dynamic range and LF/HF power ratio from spectra; see Fig. 4 of Allen et al., 2010


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% some constants
% if ispc;
% 	
%     code_path = 'Z:\hcp\rest\analysis\CODE_mot\';
% else	
%     code_path = '/export/mialab/hcp/rest/analysis/CODE_mot';   
% end;
% 
% addpath(code_path);
% addpath(genpath(code_path));
%addpath('/export/mialab/users/eswar/software/utilities')

%%
% TR = 0.74; %  sampling rate MB04
%  TR = 0.38; %  sampling rate MB04
% TR = 0.26;%  sampling rate MB04
% TR = TR;
%NyF = 1/(2*TR);
%LFband = [0, 0.10]; % LF band limits, in Hz
%HFband = [0.15, NyF]; % HF band limits, in Hz
%mp_04m = textread('/export/research/analysis/human/collaboration/olin/mialab/users/eswar/cerebro/T1_05/GICA_scripts/mp_file_list_match.txt','%s');
%mp_09m = textread('/export/research/analysis/human/collaboration/olin/mialab/users/eswar/cerebro/T1_10/GICA_scripts/mp_file_list_match.txt','%s');


TCoutname = [prefix '_timecourses_ica']; %name for TC file



%% 1. load in the Time courses from all subjects and components:


% initialize the matrix for all time courses
[nSubj,nTimePoints,nComp] = size(TCall);
nT = nTimePoints;

% Loop through subjects and load data
for ii = 1:nSubj
    TC = squeeze(TCall(ii,:,:));
    
    %TC = detrend_poly_nuis(TC,detrend_order,mp);    
    eval(['DVARS_ICATC_' prefix '_orig(:,ii) = compute_DVARS(TC);']);
   
end
% may want to save this to disk
 fprintf('saving to disk\n')
 save(fullfile(outputdir, TCoutname), 'TCall',['DVARS_ICATC_' prefix '_orig'])




%% Detrend and orthogonalize w.rto motion
detrend_order = 3;
TCdet = zeros(size(TCall));
TCdet2 = zeros(size(TCall));
for ii = 1:nSubj
    fprintf('working on subject %d of %d\n', ii, nSubj);
    
    if isempty(motpar_files)
        TCnew = detrend_poly(squeeze(TCall(ii,:,:)),detrend_order);
        if ii ==1 
            TCoutname1 = [TCoutname '_det'];
            
        end
    else
        if ii ==1 
            TCoutname1 = [TCoutname '_det_orthomot'];
            TCoutname2 = [TCoutname '_det_orthomotspike'];
        end
        mpv = squeeze(motpar_files(ii,:,:));%;load(motpar_files{ii}); 
        mpall = motpar_files;
        dmpv = [zeros(1,6);diff(mpv)];
        mp_vals = [mpv dmpv mpv.^2 dmpv.^2];
        [FDabs,FDrms] = compute_FD(mpv);
        spkInd = find(FDrms > mean(FDrms)+2.5*std(FDrms))+1;
        spkInd_all{ii} = spkInd;
        nSpk(ii) = length(spkInd);
        if ~isempty(spkInd)
            for kk=1:length(spkInd)
                mp_vals(:,24+kk)=zeros(size(mpv,1),1);
                mp_vals(spkInd(kk),24+kk)=1;
            end
        end 
        mp_vals_all{ii} = mp_vals;
        TCnew = detrend_poly_nuis(squeeze(TCall(ii,:,:)),detrend_order,mp_vals(:,1:24));
        if size(mp_vals,2) > 24
            TCnew2 = detrend_poly_nuis(squeeze(TCall(ii,:,:)),detrend_order,mp_vals);
        end
    end
    
    TCdet(ii,:,:) = TCnew;    
   
        
    
    if isempty(motpar_files)
        eval(['DVARS_ICATC_' prefix '_det(:,ii) = compute_DVARS(TCnew);']);
    else
        if size(mp_vals,2) > 24
            TCdet2(ii,:,:) = TCnew2;
            eval(['DVARS_ICATC_' prefix '_detorthomotspk(:,ii) = compute_DVARS(TCnew2);']);
        else
            TCdet2(ii,:,:) = TCnew;
            eval(['DVARS_ICATC_' prefix '_detorthomot(:,ii) = compute_DVARS(TCnew);']);
            eval(['DVARS_ICATC_' prefix '_detorthomotspk(:,ii) = compute_DVARS(TCnew);']);
        end
        
        
    end
end
save(fullfile(outputdir,'Mot_Summary.mat'),'spkInd_all','nSpk','motpar_files');

% may want to save this to disk
fprintf('saving to disk\n')
save(fullfile(outputdir,'rp24plusspike.mat'),'mp_vals_all') 
if isempty(motpar_files)
    save(fullfile(outputdir, TCoutname1), 'TCdet',['DVARS_ICATC_' prefix '_det'])
else
    %save(fullfile(outputdir, TCoutname1), 'TCdet',['DVARS_ICATC_' prefix '_detorthomot'])
    save(fullfile(outputdir, TCoutname2), 'TCdet2',['DVARS_ICATC_' prefix '_detorthomotspk'])
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Interpolate despiked TC points

if ~isempty(motpar_files) || strcmp(prefix,'motreg')
    if strcmp(prefix,'motreg')
        TCoutname3 = [TCoutname '_det_spikei'];
        %eval(['DVARS_ICATC_' prefix '_detspki = DVARS_ICATC_' prefix '_detspk;']);
    else
        TCoutname3 = [TCoutname '_det_orthomotspikei'];
        eval(['DVARS_ICATC_' prefix '_detorthomotspki = DVARS_ICATC_' prefix '_detorthomotspk;']);
    end
    TCdet3 = TCdet2;
    for ii = 1:nSubj
        mp_vals = mp_vals_all{ii};
        if size(mp_vals,2) > 24
            [sInd,~] = find(mp_vals(:,25:end) > 0); %get spike indices
            TC = squeeze(TCdet2(ii,:,:));
            TCi = interp_despiked_timecourses(TC,sInd);
            TCdet3(ii,:,:) = TCi;
            if ~strcmp(prefix,'motreg')
                eval(['DVARS_ICATC_' prefix '_detorthomotspki(:,ii) = compute_DVARS(TCi);']);
            end
        end
    end
    if ~strcmp(prefix,'motreg')
        save(fullfile(outputdir, TCoutname3), 'TCdet3',['DVARS_ICATC_' prefix '_detorthomotspki'])
    else
        save(fullfile(outputdir, TCoutname3), 'TCdet3')
    end
end
            

