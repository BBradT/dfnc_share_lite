mkdir outputfolder;
find ./ -name "*.png" -exec convert {} -trim outputfolder/{} \;
cp outputfolder/*.png .;
rm -r outputfolder;
