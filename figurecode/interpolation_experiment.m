clear;
clc;
load data/fbirn_tc
load data/parameters/DEMO_ICA_fBIRNp3_New
addpath(genpath('matcode/mi'));
%% init
S = size(TC,1);
T = size(TC,2);
NC = size(TC,3);
NCC = 1081;

%%% Interpolation Parameters
n1 = 2;
nd = 1;
n2 = 10;
toN = round(repmat(T,1,length(n1:nd:n2)).*[n1:nd:n2]);  % Interpolation Targets
toN = [T T*2 T*3 T*4 T*5 T*6 T*7 T*8 T*9 T*10];
LtoN = length(toN);

myinterp = @(x,n, method) interp1(1:T,x,linspace(1,159,n),method);

InterpMethods = {'fourier', 'linear','nearest','next','previous','pchip','makima','spline'};
%InterpMethods = {'linear','spline'};
InterpFuncs = cell(1,length(InterpMethods));
for im = 1:length(InterpFuncs)
   if strcmp(InterpMethods{im},'fourier')
       InterpFuncs{im} = @(x,n) interpft(x,n);
   else
       InterpFuncs{im} = @(x,n) myinterp(x,n,InterpMethods{im}); 
   end
end
MeasureMethods = {'MI','NMI','CORR'};
MeasureFuncs = {...
    @(x,y) mutualinfo(x,y), ...
    @(x,y) mutualinfo(x,y)/min(entropy(x),entropy(y)), ...
    @(x,y) corr(x',y') ...
    };
%% The experiment



for in = 1:LtoN
   n = toN(in);
   ntic = tic;
   fprintf('Interpolation to %d timepoints\n', n);
   for imethod = 1:length(InterpMethods)
        MethodMeasureResult = zeros( ...
        length(MeasureMethods), ...
        S, ...
        NCC);
       methodtic = tic;
       method = InterpMethods{imethod};
       fprintf('\tDoing method %s\n',method);
       TCI = zeros(S,n,NC);
       if n ~= T
           for i = 1:S
               %fprintf('\tInterpolating subject %d\n',i);
               for nc = 1:NC
                   TCI(i, :, nc) = InterpFuncs{imethod}(squeeze(TC(i,:,nc)), n);
               end
           end
       else
           TCI = TC;
       end
       %fprintf('***\n');
       for imeas = 1:length(MeasureMethods)
           measuretic = tic;
           measure = MeasureMethods{imeas};
           fprintf('\t\tMeasuring with measure %s\n',measure);
           measurefunc = MeasureFuncs{imeas};
           parfor i = 1:S
               result = zeros(NC,NC);
              %fprintf('\t\tSubject %d\n',i);
               for nc1 = 1:NC
                   tc1 = squeeze(TCI(i,:,nc1));
                   for nc2 = 1:NC
                       if nc2 < nc1 
                           continue;
                       end
                       tc2 = squeeze(TCI(i,:,nc2));
                       res = measurefunc(tc1,tc2);
                       if res == Inf
                           res = 0;
                       end
                       result(nc1,nc2) = res;
                       result(nc2,nc1) = res;
                   end
               end
               MethodMeasureResult(imeas, i, :) = mat2vec(result);
           end
           fprintf('\t\t%s took %f seconds\n', measure, toc(measuretic));
       end
       save(sprintf('results/interpolation_%s_%d.mat', method, n),'method','n','MethodMeasureResult');
       fprintf('\t%s took %f seconds\n',method,toc(methodtic));
   end
   fprintf('N=%d took %f seconds\n',n,toc(ntic));
   
end