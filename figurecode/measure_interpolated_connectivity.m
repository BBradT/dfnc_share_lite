load data/fbirn_tc
load data/parameters/DEMO_ICA_fBIRNp3_New
warning('off','MATLAB:Java:DuplicateClass');
javaaddpath('matcode/infodynamics-dist-1.5/infodynamics.jar');
addpath('matcode/infodynamics-dist-1.5/demos/octave');

%entropyCalc = javaObject('infodynamics.measures.continuous.kernel.EntropyCalculatorKernel');
%entropyCalc.setProperty('NORMALISE', 'false');
%entropyCalc.initialise();
%miCalc = javaObject('infodynamics.measures.continuous.kernel.MutualInfoCalculatorMultiVariateKernel');
%miCalc.setProperty('NORMALISE', 'false');
%miCalc.initialise();
%% init
interpN = [159, 500, 1000, 1500, 2000];
Ln = length(interpN);
S = size(TC,1);
T = size(TC,2);
NC = size(TC,3);

CorrHc = cell(1,Ln);
MiHc = cell(1,Ln);
NmiHc = cell(1,Ln);
CorrSz = cell(1,Ln);
MiSz = cell(1,Ln);
NmiSz = cell(1,Ln);
%% Methods
myinterp = @(x,n, method) interp1(1:T,x,linspace(1,159,n),method);
method = 'fourier';
InterpMethods = {'fourier', 'linear','nearest','next','previous','pchip','makima','spline'};
%InterpMethods = {'linear','spline'};
InterpFuncs = cell(1,length(InterpMethods));
for im = 1:length(InterpFuncs)
   if strcmp(InterpMethods{im},'fourier')
       InterpFuncs{im} = @(x,n) interpft(x,n);
   else
       InterpFuncs{im} = @(x,n) myinterp(x,n,InterpMethods{im}); 
   end
end
IndexC = strfind(InterpMethods,method);
interpFunc = InterpFuncs{find(not(cellfun('isempty',IndexC)))};

%% The Experiment

for in = 1:Ln
    n = interpN(in);
    TCI = zeros(S,n,NC);
    fprintf('Interpolating to %d...\n', n);
    if n ~= T
       for i = 1:S
           %fprintf('\tInterpolating subject %d\n',i);
           for nc = 1:NC
               TCI(i, :, nc) = interpFunc(squeeze(TC(i,:,nc)), n);
           end
       end
    else
       TCI = TC;
    end
    sz=TCI(DEMO.full(:,3)==1,:,:);
    hc=TCI(DEMO.full(:,3)==0,:,:);
    Lhc = size(hc,1);
    Lsz = size(sz,1);
    % Healhty Controls
    Rhc = zeros(Lhc,NC,NC);
    Mhc = zeros(Lhc,NC,NC);
    Nhc = zeros(Lhc,NC,NC);
    % Patients
    Rsz = zeros(Lsz,NC,NC);
    Msz = zeros(Lsz,NC,NC);
    Nsz = zeros(Lsz,NC,NC);
    fprintf('\thealthy controls...\n');
    parfor i = 1:Lhc
        warning('off','MATLAB:Java:DuplicateClass');
        javaaddpath('matcode/infodynamics-dist-1.5/infodynamics.jar');
        addpath('matcode/infodynamics-dist-1.5/demos/octave');
        entropyCalc = javaObject('infodynamics.measures.continuous.kernel.EntropyCalculatorKernel');
        entropyCalc.setProperty('NORMALISE', 'false');
        entropyCalc.initialise();
        miCalc = javaObject('infodynamics.measures.continuous.kernel.MutualInfoCalculatorMultiVariateKernel');
        miCalc.setProperty('NORMALISE', 'false');
        miCalc.initialise();
        R = zeros(NC);
        M = zeros(NC);
        N = zeros(NC);
        s1 = squeeze(hc(i,:,:));
        %fprintf('\t\tPre-estimating entropy for subject %d...\n', i);
        E = zeros(1,NC);
        for k1 = 1:NC
            ss1 = squeeze(s1(:,k1));
            entropyCalc.setObservations(ss1);
            E(k1) = entropyCalc.computeAverageLocalOfObservations();
        end
        fprintf('\t\tEstimating Components for subject %d...\n',i);
        for k1 = 1:NC
            ss1 = squeeze(s1(:,k1));
            for k2 = 1:NC
                if k2 <= k1
                    continue;
                end
                ss2 = squeeze(s1(:,k2));
                PCC = corr(ss1,ss2);
                R(k1,k2) = PCC;
                miCalc.setObservations(ss1, ss2);
                mi = miCalc.computeAverageLocalOfObservations();;
                nmi = mi/min(E(k1),E(k2));
                M(k1,k2)= mi;
                N(k1,k2) = nmi;
            end
        end
        Rhc(i,:,:) = R;
        Mhc(i,:,:) = M;
        Nhc(i,:,:) = N;
        
        %fprintf('Done testing HC subject %d\n', i);
    end
    parfor i = 1:Lhc
        Mhc(i,:, :) = triu(squeeze(Mhc(i,:,:)),1)';
        Nhc(i,:, :) = triu(squeeze(Nhc(i,:,:)),1)';        
        Rhc(i,:, :) = triu(squeeze(Rhc(i,:,:)),1)';        
    end
    fprintf('\tpatients...\n');
    parfor i = 1:Lsz
        warning('off','MATLAB:Java:DuplicateClass');
        javaaddpath('matcode/infodynamics-dist-1.5/infodynamics.jar');
        addpath('matcode/infodynamics-dist-1.5/demos/octave');
        entropyCalc = javaObject('infodynamics.measures.continuous.kernel.EntropyCalculatorKernel');
        entropyCalc.setProperty('NORMALISE', 'false');
        entropyCalc.initialise();
        miCalc = javaObject('infodynamics.measures.continuous.kernel.MutualInfoCalculatorMultiVariateKernel');
        miCalc.setProperty('NORMALISE', 'false');
        miCalc.initialise();
        R = zeros(NC);
        M = zeros(NC);
        N = zeros(NC);
        s1 = squeeze(sz(i,:,:));
        %fprintf('\t\tPre-estimating entropy for subject %d...\n', i);
        E = zeros(1,NC);
        for k1 = 1:NC
            ss1 = squeeze(s1(:,k1));
            entropyCalc.setObservations(ss1);
            E(k1) = entropyCalc.computeAverageLocalOfObservations();
        end
        fprintf('\t\tEstimating Components for subject %d...\n',i);
        for k1 = 1:NC
            ss1 = squeeze(s1(:,k1));           
            for k2 = 1:NC
                if k2 < k1
                    continue;
                end
                ss2 = squeeze(s1(:,k2));
                PCC = corr(ss1,ss2);
                R(k1,k2) = PCC;
                miCalc.setObservations(ss1, ss2);
                mi = miCalc.computeAverageLocalOfObservations();
                nmi = mi/min(E(k1),E(k2));
                M(k1,k2)= mi;
                N(k1,k2) = nmi;
            end
        end
        Rsz(i,:,:) = R;
        Msz(i,:,:) = M;
        Nsz(i,:,:) = N;
        %fprintf('Done testing SZ subject %d\n', i);
    end
    parfor i = 1:Lsz
        Msz(i,:, :) = triu(squeeze(Msz(i,:,:)),1)';
        Nsz(i,:, :) = triu(squeeze(Nsz(i,:,:)),1)';        
        Rsz(i,:, :) = triu(squeeze(Rsz(i,:,:)),1)';        
    end
    save(sprintf('results/interpolated_connectivity_%d_%s.mat', n, method), 'Rhc', 'Rsz', 'Mhc', 'Msz', 'Nhc', 'Nsz');
end
