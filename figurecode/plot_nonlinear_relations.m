close all;
load data/fbirn_tc.mat
sz=TC(DEMO.full(:,3)==1,:,:);
hc=TC(DEMO.full(:,3)==0,:,:);
tc1 = squeeze(hc(1,:,14));
tcf1 = uint8_preservable(tc1,10);

params = {
    'LineWidth', 2
    };

bs = 1:1:10;
cmap = jet(length(bs))/1.25;
r = 1;
correlations = zeros(r,length(bs));
mis = zeros(r,length(bs));
nmis = zeros(r,length(bs));
legs = cell(1,length(bs));
figure;
hold on;
for i = 1:length(bs)
    b = bs(i);
    yl = tc1 * b + b;

    TC = uint8_preservable([tc1;yl],10);
    tcf1 = TC(1,:);
    y = TC(2,:);
    scatter(uint8(tcf1),uint8(y),20,cmap(i,:),params{:});
    correlations(i) = corr(y',tcf1');
    legs{i} = sprintf('$b$=%d',b);
    [mis(i), nmis(i)] = RMI(uint8(y),uint8(tcf1));
    bhat = y/tcf1;
    yhat = tcf1 * bhat;
end
    
xlabel('$x$','interpreter','latex');
ylabel('$bx+b$','interpreter','latex');
set(gca,'FontSize',18);
legend(legs,'fontsize',12,'location','best','interpreter','latex');
grid on;
grid minor;
saveas(gcf,'figures/linear_relations.png');

figure;
plot(bs,correlations,'r-o',params{:});
hold on;
plot(bs,mis,'b-o',params{:});
plot(bs,nmis,'g-o',params{:});
set(gca,'FontSize',18);
ylim([0,max(mis)]);
grid on;
grid minor;
xlim([min(bs), max(bs)]);
ylabel('$d(x,bx+b)$','interpreter','latex');
xlabel('$b$','interpreter','latex');
legend({'Correlation','MI','NMI'},'location','best','fontsize',12);
saveas(gcf,'figures/linear_measures.png');

% Nonlinear

bs = 1:1:10;
cmap = jet(length(bs))/1.25;
r = 1;
correlations = zeros(r,length(bs));
mis = zeros(r,length(bs));
nmis = zeros(r,length(bs));
legs = cell(1,length(bs));
figure;
hold on;
colormap colorcube;
for i = 1:length(bs)
    b = bs(i);
    yl = b*tc1.^2 + tc1 + b;
    TC = uint8_preservable([tc1;yl],10);
    tcf1 = TC(1,:);
    y = TC(2,:);
    legs{i} = sprintf('$b$=%d',b);
    scatter(uint8(tcf1),uint8(y),20,cmap(i,:),params{:});
    correlations(i) = corr(y',tcf1');
    [mis(i), nmis(i)] = RMI(uint8(y),uint8(tcf1));
end

xlabel('$x$','interpreter','latex');
ylabel('$bx^2+x+b$','interpreter','latex');
set(gca,'FontSize',18);
legend(legs,'fontsize',12,'location','best','interpreter','latex');
grid on;
grid minor;
saveas(gcf,'figures/nonlinear_relations.png');

figure;
plot(bs,correlations,'r-o',params{:});
hold on;
plot(bs,mis,'b-o',params{:});
plot(bs,nmis,'g-o',params{:});
set(gca,'FontSize',18);
ylim([0,max(mis)]);
grid on;
grid minor;
xlim([min(bs), max(bs)]);
xlabel('$b$','interpreter','latex');
ylabel('$d(x,bx^2+x+b)$','interpreter','latex');
legend({'Correlation','MI','NMI'},'location','best','fontsize',12);
saveas(gcf,'figures/nonlinear_measures.png');


% Cubed

bs = 1:1:10;
cmap = jet(length(bs))/1.25;
r = 1;
correlations = zeros(r,length(bs));
mis = zeros(r,length(bs));
nmis = zeros(r,length(bs));
legs = cell(1,length(bs));
figure;
hold on;
colormap colorcube;
for i = 1:length(bs)
    b = bs(i);
    yl = b*tc1.^(2) + b;
    TC = uint8_preservable([tc1;yl],10);
    tcf1 = TC(1,:);
    y = TC(2,:);
    legs{i} = sprintf('$b$=%d',b);
    scatter(uint8(tcf1),uint8(y),20,cmap(i,:),params{:});
    correlations(i) = corr(y',tcf1');
    [mis(i), nmis(i)] = RMI(uint8(y),uint8(tcf1));
end

xlabel('$x$','interpreter','latex');
ylabel('$bx^2+b$','interpreter','latex');
set(gca,'FontSize',18);
legend(legs,'fontsize',12,'location','best','interpreter','latex');
grid on;
grid minor;
saveas(gcf,'figures/nonlinear2_relations.png');

figure;
plot(bs,correlations,'r-o',params{:});
hold on;
plot(bs,mis,'b-o',params{:});
plot(bs,nmis,'g-o',params{:});
set(gca,'FontSize',18);
ylim([min(correlations),max(mis)]);
grid on;
grid minor;
xlim([min(bs), max(bs)]);
xlabel('$b$','interpreter','latex');
ylabel('$d(x,bx^2+b)$','interpreter','latex');
legend({'Correlation','MI','NMI'},'location','best','fontsize',12);
saveas(gcf,'figures/nonlinear2_measures.png');


tilefigs;

