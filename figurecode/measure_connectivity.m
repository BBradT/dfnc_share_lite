load data/fbirn_tc
load data/parameters/DEMO_ICA_fBIRNp3_New

%% init
sz=TC(DEMO.full(:,3)==1,:,:);
hc=TC(DEMO.full(:,3)==0,:,:);

Lsz = size(sz,1);
Lhc = size(hc,1);
T = size(sz,2);
NC = size(sz,3);

% Healhty Controls
Phc = zeros(Lhc,NC,NC);
Rhc = zeros(Lhc,NC,NC);
Mhc = zeros(Lhc,NC,NC);
Nhc = zeros(Lhc,NC,NC);
Jhc = zeros(Lhc,NC,NC);
Fhc = zeros(NC,NC);
% Patients
Psz = zeros(Lsz,NC,NC);
Rsz = zeros(Lsz,NC,NC);
Msz = zeros(Lsz,NC,NC);
Nsz = zeros(Lsz,NC,NC);
Jsz = zeros(Lsz,NC,NC);
Fsz = zeros(NC,NC);

for i = 1:Lhc
    s1 = squeeze(hc(i,:,:));
    for k1 = 1:NC
        ss1 = uint8_preservable(squeeze(s1(:,k1)),10);
        for k2 = 1:NC
            if k2 > k1
                continue;
            end
            ss2 = uint8_preservable(squeeze(s1(:,k2)),10);
            [PCC,PV,RL,RU] = corrcoef(ss1,ss2);
            PCC = min(min(PCC));
            PV = min(min(PV));
            Rhc(i,k1,k2) = PCC;
            Rhc(i,k2,k1) = PCC;
            Phc(i,k1,k2) = PV;
            Phc(i,k2,k1) = PV;           
            [mi, nmi, e1, e2, je] = RMI(uint8(ss1),uint8(ss2));
            Mhc(i,k1,k2)= mi;
            Nhc(i,k1,k2) = nmi;
            Jhc(i,k1,k2) = je;
            Mhc(i,k2,k1)= mi;
            Nhc(i,k2,k1) = nmi;
            Jhc(i,k2,k1) = je;

        end
    end
    fprintf('Done testing HC subject %d\n', i);
end

for i = 1:Lsz
    s1 = squeeze(sz(i,:,:));
    for k1 = 1:NC
        ss1 = uint8_preservable(squeeze(s1(:,k1)),10);
        for k2 = 1:NC
            if k2 > k1
                continue;
            end
            ss2 = uint8_preservable(squeeze(s1(:,k2)),10);
            [PCC,PV,RL,RU] = corrcoef(ss1,ss2);
            PCC = min(min(PCC));
            PV = min(min(PV));
            Rsz(i,k1,k2) = PCC;
            Rsz(i,k2,k1) = PCC;
            Psz(i,k1,k2) = PV;
            Psz(i,k2,k1) = PV;           
            [mi, nmi, e1, e2, je] = RMI(uint8(ss1),uint8(ss2));
            Msz(i,k1,k2)= mi;
            Nsz(i,k1,k2) = nmi;
            Jsz(i,k1,k2) = je;
            Msz(i,k2,k1)= mi;
            Nsz(i,k2,k1) = nmi;
            Jsz(i,k2,k1) = je;
        end
    end
    fprintf('Done testing SZ subject %d\n', i);
end

save('results/nonlinear_final.mat');