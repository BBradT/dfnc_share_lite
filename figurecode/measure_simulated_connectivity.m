load data/simulated_tc
%warning('off','MATLAB:Java:DuplicateClass');
%javaaddpath('matcode/infodynamics-dist-1.5/infodynamics.jar');
%addpath('matcode/infodynamics-dist-1.5/demos/octave');
addpath(genpath('matcode/mi'));
%% init
S = size(TC,1);
T = size(TC,2);
NC = size(TC,3);

%% The Experiment

    hc=TC;
    L = size(hc,1);
    L = 64;
    % Healhty Controls
    Corr = zeros(L,NC,NC);
    Mi = zeros(L,NC,NC);
    Nmi = zeros(L,NC,NC);
    fprintf('\thealthy controls...\n');
    for i = 1:L
        %warning('off','MATLAB:Java:DuplicateClass');
        %javaaddpath('matcode/infodynamics-dist-1.5/infodynamics.jar');
        %addpath('matcode/infodynamics-dist-1.5/demos/octave');
        %entropyCalc = javaObject('infodynamics.measures.continuous.kernel.EntropyCalculatorKernel');
        %entropyCalc.setProperty('NORMALISE', 'false');
        %entropyCalc.initialise();
        %miCalc = javaObject('infodynamics.measures.continuous.kernel.MutualInfoCalculatorMultiVariateKernel');
        %miCalc.setProperty('NORMALISE', 'false');
        %miCalc.initialise();
        C = zeros(NC);
        M = zeros(NC);
        N = zeros(NC);
        s1 = squeeze(hc(i,:,:));
        %fprintf('\t\tPre-estimating entropy for subject %d...\n', i);
        E = zeros(1,NC);
        for k1 = 1:NC
            ss1 = squeeze(s1(:,k1));
            %entropyCalc.setObservations(ss1);
            %E(k1) = entropyCalc.computeAverageLocalOfObservations();
            E(k1) = entropy(ss1);
        end
        fprintf('\t\tEstimating Components for subject %d...\n',i);
        for k1 = 1:NC
            ss1 = squeeze(s1(:,k1));
            for k2 = 1:NC
                if k2 <= k1
                    continue;
                end
                ss2 = squeeze(s1(:,k2));
                PCC = corr(ss1,ss2);
                C(k1,k2) = PCC;
                %miCalc.setObservations(ss1, ss2);
                %mi = miCalc.computeAverageLocalOfObservations();;
                mi = mutualinfo(ss1,ss2);
                nmi = mi/min(E(k1),E(k2));
                M(k1,k2)= mi;
                N(k1,k2) = nmi;
            end
        end
        Corr(i,:,:) = C;
        Mi(i,:,:) = M;
        Nmi(i,:,:) = N;
        
        %fprintf('Done testing HC subject %d\n', i);
    end
    parfor i = 1:L
        Mi(i,:, :) = triu(squeeze(Mi(i,:,:)),1)';
        Nmi(i,:, :) = triu(squeeze(Nmi(i,:,:)),1)';        
        Corr(i,:, :) = triu(squeeze(Corr(i,:,:)),1)';        
    end

    save(sprintf('results/simulated_connectivity.mat'), 'Corr', 'Mi', 'Nmi');
