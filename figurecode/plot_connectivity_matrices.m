load results/nonlinear_final.mat;

%% Connectivity
% In this section, our aim is to investigate how Mutual-Information based
% measures can provide insights into differences between patient and
% healhty control connectivity. We first perform a high-level analysis of
% the differences in the resulting connectivity matrices, noting how the
% structure of the matrices is altogether 

[L, RSN, Art, MOD] = comp_labels_fb_C100_vn_FINAL;
L = L(RSN);

%%%
% First, we plot the static connectivity matrices using the standard
% correlation metric. We compute the correlation between the 47 
% Independent Components (ICs) estimated in Damaraju et al. 2015, and
% provide connectivity plots for each population separately.
% We also plot the mean P-values over the entire population for computating.
% correlation of each components

%%%
% Next, we plot the static connectivity matrices using the mutual
% information. We compute mutual information between the 47 
% Independent Components (ICs) estimated in Damaraju et al. 2015, and
% provide connectivity plots for each population separately.

%%%
% Then, we plot the static connectivity matrices using normalized mutual
% information. We compute normalized mutual information between the 47 
% Independent Components (ICs) estimated in Damaraju et al. 2015, and
% provide connectivity plots for each population separately.


mRhc = squeeze(mean(Rhc,1));
mRhc(1:1+size(mRhc,1):end) = min(min(mRhc));

mRsz = squeeze(mean(Rsz,1));
mRsz(1:1+size(mRsz,1):end) = min(min(mRsz));

Lo=min(min(min(mRhc)),min(min(mRsz)));
Hi=max(max(max(mRhc)),max(max(mRsz)));

[fig, A, C, I] = plot_FNC(mRhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Control Standard Static Functional Network Connectivity using Correlation.');
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'figures/Corr_HC.png');

[fig, A, C, I] = plot_FNC(mRsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Control Standard Static Functional Network Connectivity using Correlation.');
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'figures/Corr_SZ.png');

mMhc = squeeze(mean(Mhc,1));
mMhc(1:1+size(mMhc,1):end) = min(min(mMhc));
mMsz = squeeze(mean(Msz,1));
mMsz(1:1+size(mMsz,1):end) = min(min(mMsz));

Lo=min(min(min(mMhc)),min(min(mMsz)));
Hi=max(max(max(mMhc)),max(max(mMsz)));

[fig, A, C, I] = plot_FNC(mMhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Control Static Functional Network Connectivity using Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'figures/Mi_HC.png');

[fig, A, C, I] = plot_FNC(mMsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Patient Static Functional Network Connectivity using Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'figures/Mi_SZ.png');

% Next, we plot Normalized Mutual Information
mNhc = squeeze(mean(Nhc,1));
mNhc(1:1+size(mNhc,1):end) = min(min(mNhc));
mNsz = squeeze(mean(Nsz,1));
mNsz(1:1+size(mNsz,1):end) = min(min(mNsz));
Lo=min(min(min(mNhc)),min(min(mNsz)));
Hi=max(max(max(mNhc)),max(max(mNsz)));

[fig, A, C, I] = plot_FNC(mNhc,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Healthy Static Functional Network Connectivity using Normalized Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'figures/Nmi_HC.png');

[fig, A, C, I] = plot_FNC(mNsz,[Lo,Hi],L,1:47,'w','',MOD,'jet');
title('Figure: Patient Static Functional Network Connectivity using Normalized Mutual Information.');
set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
set(get(gca,'title'),'Position',[NC/2 NC+7 1.00011]);
saveas(gcf,'figures/Nmi_SZ.png');

