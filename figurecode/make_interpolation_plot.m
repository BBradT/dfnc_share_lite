clear;
clc;
close all;
load data/fbirn_tc
load data/parameters/DEMO_ICA_fBIRNp3_New
addpath(genpath('matcode/mi'));
addpath(genpath('matcode/linspecer'));
%% init
S = size(TC,1);
T = size(TC,2);
NC = size(TC,3);
NCC = 1081;

%%% Interpolation Parameters
toN = [T T*2 T*3 T*4 T*5 T*6 T*7 T*8 T*9 T*10];
LtoN = length(toN);

InterpMethods = {'fourier', 'linear','nearest','next','previous','pchip','cubic','makima','spline'};
LInterp = length(InterpMethods);
MeasureMethods = {'MI','NMI','CORR'};
LMeas = length(MeasureMethods);
%% Gather Results
%%% We want to measure how the mean NMI/MI changes (for each component?)
%%% with n. Thus, we plot a surface with x=1:n, y=1:1081, and z=change in
%%% measure? Or, we make a 2-D plot with x=1:n, and y=mean change in
%%% measure.

Nfull = zeros(length(InterpMethods), LtoN, NCC);
Nmean = zeros(length(InterpMethods), LtoN);
Mfull = zeros(length(InterpMethods), LtoN, NCC);
Mmean = zeros(length(InterpMethods), LtoN);
Cfull = zeros(length(InterpMethods), LtoN, NCC);
Cmean = zeros(length(InterpMethods), LtoN);

for imethod = 1:LInterp
    method = InterpMethods{imethod};
    for in = 1:LtoN
        n = toN(in);
        result = load(sprintf('results/interpolation_%s_%d.mat',method,n));
        result.MethodMeasureResult(isnan(result.MethodMeasureResult)) = 0;
        result.MethodMeasureResult(isinf(result.MethodMeasureResult)) = 0;
        meanRes = squeeze(mean(result.MethodMeasureResult,2));
        
        Mfull(imethod, in, :) = meanRes(1,:);
        Nfull(imethod, in, :) = meanRes(2,:);
        Cfull(imethod, in, :) = meanRes(3,:);
        Mmean(imethod, in) = squeeze(mean(meanRes(1,:)));
        Nmean(imethod, in) = squeeze(mean(meanRes(2,:)));
        Cmean(imethod, in) = squeeze(mean(meanRes(3,:)));
    end
end

%% Plot
plotparams = {'LineWidth', 2};
axparams = {'FontSize', 18};
legparams = {'FontSize', 12, 'Location', 'Best'};

cols = linspecer(LInterp);

figure;

for imethod = 1:LInterp
    subplot(1,3,1);
    plot(toN, squeeze(Mmean(imethod,:)), 'color', cols(imethod,:), plotparams{:});
    hold on;
    subplot(1,3,2);
    plot(toN, squeeze(Nmean(imethod,:)), 'color', cols(imethod,:), plotparams{:});
    hold on;
    subplot(1,3,3);
    plot(toN, squeeze(Cmean(imethod,:)), 'color', cols(imethod,:), plotparams{:});
    hold on;
end
subplot(1,3,1);
title('Mutual Information');
grid on;
grid minor;
xlabel('$N$','interpreter','latex');
ylabel('$\mu(MI)$','interpreter','latex');
set(gca,axparams{:});
legend(InterpMethods,legparams{:});
subplot(1,3,2);
title('NMI');
grid on;
grid minor;
xlabel('$N$','interpreter','latex');
ylabel('$\mu(NMI)$','interpreter','latex');
set(gca,axparams{:});
legend(InterpMethods,legparams{:});
subplot(1,3,3);
title('Correlation');
grid on;
grid minor;
xlabel('$N$','interpreter','latex');
ylabel('$\mu(PCC)$','interpreter','latex');
set(gca,axparams{:});
legend(InterpMethods,legparams{:});

set(gcf, 'Units', 'Normalized', 'OuterPosition',[0,0,1,1]);
saveas(gcf,'figures/Mean_Interpolation.png');