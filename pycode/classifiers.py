from polyssifier import Polyssifier
import scipy.io as sio
import os
import numpy as np
from sklearn.svm import LinearSVC
from sklearn.linear_model import LassoCV
from sklearn.feature_selection import SelectFromModel, VarianceThreshold, SelectKBest, chi2, mutual_info_classif, f_classif

wd = '/home/bbaker/projects/dfnc_share_lite'
N = 314
inds = np.arange(N)
np.random.shuffle(inds)
Rx = sio.loadmat(os.path.join(wd,'data','Rx.mat'))
var = 0.2
vrs = VarianceThreshold(threshold=(var*(1-var)))
NF = 10
kb = 100
include = [
    'Logistic Regression',
    'Random Forest',
    'SVM',
    'Multilayer Perceptron'
        ]
if not os.path.exists('polyR'):
   os.makedirs('polyR')

X = Rx['X'][inds,:]
y = Rx['y'][inds]
y = y.flatten()
lsvc = LinearSVC(C=0.1,penalty='l1',dual=False).fit(X,y)
model = SelectFromModel(lsvc,prefit=True)
Xn = model.transform(X)
#Xn = SelectKBest(mutual_info_classif,k=kb).fit_transform(X,y)
polyR = Polyssifier(Xn,y,project_name='polyR',n_folds=NF,path='.',include=include)
polyR.build()
polyR.run()

del Rx
del polyR

Mx = sio.loadmat(os.path.join(wd,'data','Mx.mat'))
X = Mx['X'][inds,:]
y = Mx['y'][inds]
y = y.flatten()
lsvc = LinearSVC(C=0.1,penalty='l1',dual=False).fit(X,y)
model = SelectFromModel(lsvc,prefit=True)
Xn = model.transform(X)
#Xn = SelectKBest(mutual_info_classif,k=kb).fit_transform(X,y)

if not os.path.exists('polyM'):
   os.makedirs('polyM')
polyM = Polyssifier(Xn,y,project_name='polyM',n_folds=NF,path='.',include=include)
polyM.build()
polyM.run()

del Mx
del polyM

Nx = sio.loadmat(os.path.join(wd,'data','Nx.mat'))
X = Nx['X'][inds,:]
y = Nx['y'][inds]
y = y.flatten()
lsvc = LinearSVC(C=0.1,penalty='l1',dual=False).fit(X,y)
model = SelectFromModel(lsvc,prefit=True)
Xn = model.transform(X)
#Xn = SelectKBest(mutual_info_classif,k=kb).fit_transform(X,y)
if not os.path.exists('polyN'):
   os.makedirs('polyN')
polyN = Polyssifier(Xn,y,project_name='polyN',n_folds=NF,path='.',include=include)
polyN.build()
polyN.run()
del Nx
del polyN

RMx = sio.loadmat(os.path.join(wd,'data','RMx.mat'))
X = RMx['X'][inds,:]
y = RMx['y'][inds]
y = y.flatten()
lsvc = LinearSVC(C=0.1,penalty='l1',dual=False).fit(X,y)
model = SelectFromModel(lsvc,prefit=True)
Xn = model.transform(X)
#Xn = SelectKBest(mutual_info_classif,k=kb).fit_transform(X,y)
if not os.path.exists('polyRM'):
   os.makedirs('polyRM')
polyRM = Polyssifier(Xn,y,project_name='polyRM',n_folds=NF,path='.',include=include)
polyRM.build()
polyRM.run()
del RMx
del polyRM

RNx = sio.loadmat(os.path.join(wd,'data','RNx.mat'))
X = RNx['X'][inds,:]
y = RNx['y'][inds]
y = y.flatten()
lsvc = LinearSVC(C=0.1,penalty='l1',dual=False).fit(X,y)
model = SelectFromModel(lsvc,prefit=True)
Xn = model.transform(X)
#Xn = SelectKBest(mutual_info_classif,k=kb).fit_transform(X,y)
if not os.path.exists('polyRN'):
   os.makedirs('polyRN')
polyRN = Polyssifier(Xn,y,project_name='polyRN',n_folds=NF,path='.',include=include)
polyRN.build()
polyRN.run()
del RNx
del polyRN


