import pickle as pk
import os
import pandas as pd
import seaborn as sb
import matplotlib.pyplot as plt

methods = {
        'polyR': 'Correlation',
        'polyM': 'MI',
        'polyN': 'NMI',
        'polyRM': 'Corr + MI',
        'polyRN': 'Corr + NMI'
}
sb.set()
clist = []
slist = []
mlist = []
for mfolder in methods.keys():
    method = methods[mfolder]
    scores = pk.load(open(os.path.join(mfolder, 'scores.pkl'),'rb'))
    for classifier in scores.keys():
        if 'test' not in classifier:
            continue
        if 'voting' in classifier.lower():
            continue
        cscores = scores[classifier]
        classifier = classifier.replace('test','').strip()
        #data.append(pd.DataFrame({'classifier':classifier, 'metric':method, 'score':score}))
        for score in cscores:
            clist.append(classifier)
            mlist.append(method)
            slist.append(score)
data = pd.DataFrame({'classifier':clist,'score':slist,'metric':mlist})

sb.boxplot(x='classifier', y='score', data=data, hue='metric')
plt.show()
