import scipy.io as spio
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
#sns.set(style="darkgrid")

def generatePlotData(eg, er, je, mi, nmi):
	m_eg = np.mean(eg, axis=1)
	sd_eg = np.std(eg, axis=1)
	m_er = np.mean(er, axis=1)
	sd_er = np.std(er, axis=1)
	m_je = np.mean(je, axis=1)
	sd_je = np.std(je, axis=1)
	m_mi = np.mean(mi, axis=1)
	sd_mi = np.std(mi, axis=1)
	m_nmi = np.mean(nmi, axis=1)
	sd_nmi = np.std(nmi, axis=1)
	
	return m_eg, sd_eg, m_er, sd_er, m_je, sd_je, m_mi, sd_mi, m_nmi, sd_nmi



def main():
	mat = spio.loadmat('time_validation_160_overlap2.mat', squeeze_me=True)

	entrpy_green= mat['entrpy_green'] 
	entrpy_red= mat['entrpy_red']
	joint_entrpy= mat['joint_entrpy'] 
	mutual= mat['mutual']
	Normalized_MI= mat['Normalized_MI'] 

	(m_eg, sd_eg, m_er, sd_er, m_je, sd_je, m_mi, sd_mi, m_nmi, sd_nmi) = generatePlotData(entrpy_green, entrpy_red,joint_entrpy,mutual,Normalized_MI)
	fig, ax1 = plt.subplots()
	color = 'tab:red'
	ax1.set_xlabel('Entropy Increase Over Time')
	ax1.set_ylabel('Entropy Scale', color=color)
	#plt.hold(1)
	plt.plot(m_eg,linestyle='--',linewidth=2.5, color= '#008000',label='Signal 1 Entropy')
	plt.fill_between(range(17), m_eg-sd_eg, m_eg+sd_eg, alpha = 0.5, )
	plt.plot(m_er,linestyle='-.',linewidth=2.5, color= '#DC143C',label='Signal 2 Entropy')
	plt.fill_between(range(17), m_er-sd_er, m_er+sd_er, alpha = 0.5,facecolor='#FA8072')
	plt.plot(m_je,linestyle='-',linewidth=2.5, color= '#8B4513',label='Joint Entropy')
	plt.fill_between(range(17), m_je-sd_je, m_je+sd_je, alpha = 0.5,facecolor='#CD853F')
	ax1.tick_params(axis='y', labelcolor=color)
	ax1.set_xticks([])
	plt.legend(loc=2)
	
	
	ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

	color = 'tab:blue'
	ax2.set_ylabel('MI and NMI Scale', color=color)  # we already handled the x-label with ax1
	plt.plot(m_mi,linestyle=':',linewidth=2.5, color= '#1E90FF',label='MI')
	plt.fill_between(range(17), m_mi-sd_mi, m_mi+sd_mi, alpha = 0.5)
	plt.plot(m_nmi,linestyle='-',linewidth=2.5, color= '#4B0082',label='NMI')
	plt.fill_between(range(17), m_nmi-sd_nmi, m_nmi+sd_nmi, alpha = 0.5,facecolor='#483D8B')
	ax2.tick_params(axis='y', labelcolor=color)
	fig.tight_layout()
	#handles, labels = ax.get_legend_handles_labels()
	#ax.legend(handles, labels)
	plt.legend(loc=4)
	#lns = lns1+lns2+lns3
	#labs = [l.get_label() for l in lns]
	#ax.legend(lns, labs, loc=0)
	fig.savefig("Time_1000_sim.pdf")
	fig.savefig("Time_1000_sim.eps")
	fig.savefig("Time_1000_sim.png")
	plt.show()
	
	
	
	
if __name__ == "__main__":
	main()