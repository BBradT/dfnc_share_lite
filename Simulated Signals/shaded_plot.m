yyaxis left
stdshade(entrpy_green',0.2,'g');
m1='Green Entropy';
hold on
stdshade(entrpy_red',0.2,'r');
m2='Red Entropy';
hold on
stdshade(joint_entrpy',0.2,'k');
m3='Joint Entropy';
hold on
% axis([1 8 0 8]);
xlabel('Cases')
ylabel('Entropy')

yyaxis right
stdshade(mutual',0.2,'b');
m4='MI';
hold on
stdshade(Normalized_MI',0.2,'m');
m5='NMI';
hold on
% axis([1 8 0 0.5]);
ylabel('MI/NMI')
legend