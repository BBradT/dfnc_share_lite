import matplotlib.pyplot as plt
plt.style.use('seaborn-white')
import numpy as np
import scipy.io as spio

def main():
	mat = spio.loadmat('time_validation_160_overlap2.mat', squeeze_me=True)
	green= mat['green'] 
	red= mat['red']
	fig = plt.figure()
	cases= [0,2,4,8,16]
	c=1;
	#fig, ax = plt.subplots(5, 1, sharex='col', sharey='row')
	for i in range(0, 5):
		
		ax = fig.add_subplot(5, 1, c)
		plt.plot(green,linewidth=2, color= 'g')
		plt.plot(red[cases[i]][0],linewidth=2, color= 'r')
		c=c+1;
		ax.set_xticks([])
			
		print (i)
	
	ax.set_xticks(np.arange(0, 170, step=10))
	ax.set_xlabel('Time')
	
	#ax.set_ylabel('Intensity')
	
	fig.savefig("signal.pdf")
	fig.savefig("Signal.eps")
	fig.savefig("Signal.png")
	plt.show()
			
	
	
		


if __name__ == "__main__":
	main()