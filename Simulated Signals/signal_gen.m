
for i=1:160

    a = 200;
    b = 255;
    r = round((b-a).*rand + a);
    green(i)=r;
    
end

count=1;
red=zeros(8,100,160);
for k=1:17

    
    for i=1:100
    
    
        for j=1:count
    
            a = 1;
            b = 160;
            pos = round((b-a).*rand + a);

            a = 1;
            b = 255;
            r = round((b-a).*rand + a);

            red(k,i,pos)=r;
        end
    end

    for i=1:100
    
        red_temp=squeeze(red(k,i,:));
        [MI,NMI,ent1,ent2, JE]=RMI(uint8(red_temp'),uint8(green));
        Normalized_MI(k,i)=NMI;
        mutual(k,i)=MI;
        entrpy_red(k,i)=ent1;
        entrpy_green(k,i)=ent2;
        joint_entrpy(k,i)=JE;
    
    
    end
    count=count+10;
end